import {ErrorCode} from './ErrorCode'
import {ErrorMessage} from '../message/ErrorMessage'

export class CustomError {
  public static builder (code: ErrorCode, message: ErrorMessage){
    return {
      errors:[{
        'rule':code,
        'message':message,
        'field':null,
      }],
    }
  }
}
