
export enum ErrorCode {
  USER_NOT_FOUND_CODE = 'user.not.found',
  UNAUTHORIZED_USER_CODE = 'unauthorized.user',
  BAD_REQUEST = 'bad.request',
  ACCOUNT_INACTIVE = 'account.inactive',
  SERVER_ERROR = 'server.error',
  NOT_FOUND = 'data.not.found',
  INVALID_TOKEN = 'invalid.or.expired.token',
  ROUTE_NOT_FOUND = 'route.not.found',
 SERVICE_UNAVAILABLE = 'service.unavailable'
}
