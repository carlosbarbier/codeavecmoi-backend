export enum AlertMessage {
  USER_LOGOUT = 'user logout successfully ',
  USER_ACTIVE = 'your account is active',
  PASSWORD_RESET = 'your password has been reseted',
  PASSWORD_UPDATED = 'your password has been updated successfully',
  EMAIL_SEND_TO_RESET_PASSWORD = 'we sent an email to reset your password',
  EMAIL_ACTIVATION = 'we sent an email to activate your account',
  CATEGORY_UPDATED = 'category has been updated',
  COURSE_CREATED = 'course created',

}
