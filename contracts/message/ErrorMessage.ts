export enum ErrorMessage {
  USER_NOT_FOUND = 'invalid email or password',
  BAD_REQUEST = 'invalid payload or request',
  ACCOUNT_INACTIVE = 'account inactive',
  INTERNAL_ERROR = 'The server encountered an internal error',
  NOT_FOUND = 'data not found',
  UNAUTHORIZED_USER = 'access restricted',
  INVALID_TOKEN = 'Invalid or expired token',
  ROUTE_NOT_FOUND = 'invalid route',
  SERVICE_UNAVAILABLE = 'service unavailable',
}
