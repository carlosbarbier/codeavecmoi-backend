import {AlertMessage} from 'Contracts/message/AlertMessage'

export class CustomAlert {
  public static builder (message: AlertMessage){
    return {
      message:[{
        'info':message,
      }],
    }
  }
}
