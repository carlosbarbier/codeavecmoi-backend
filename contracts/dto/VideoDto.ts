export interface VideoDto {
  title: string
  video_id: number
  url: string
  position?: string
  duration?: string
}


