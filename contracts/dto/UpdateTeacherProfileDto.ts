export interface UpdateTeacherProfileDto {
  name?: string,
  last_name?: string,
  about?: string,
  facebook?: string,
  youtube?:string,
  twitter?:string
  instagram?:string
  website?:string
  picture?:string
  occupation?:string
}
