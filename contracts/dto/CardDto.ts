import {StripeCardDto} from "Contracts/dto/StripeCardDto";

export interface CardDto extends StripeCardDto{
  amount:number,
  slugs:Array<string>
}
