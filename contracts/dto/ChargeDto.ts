import {Currency} from "Contracts/constant";

export interface ChargeDto {
  amount: number,
  currency: Currency,
  description: string
}
