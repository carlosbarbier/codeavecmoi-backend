export interface CreditCardDto {
  number: string,
  expiry: string,
  name:string
  cvc: string,
}
