
export interface RatingDto {
  comment_body?:string,
  rating: number
}
