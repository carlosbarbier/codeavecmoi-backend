export interface UpdateProfileDto {
  email: string
  name: string
}
