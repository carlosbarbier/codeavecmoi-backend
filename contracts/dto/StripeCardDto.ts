export interface StripeCardDto {
  number: string,
  exp_month: string,
  exp_year: string,
  cvc: string,
}
