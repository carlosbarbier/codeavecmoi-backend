import {SocialAccount} from "Contracts/constant";

export interface RegistrationDto {
  name: string
  password: string
  email: string
  role?:string
  source?:SocialAccount
  token?:string
  is_active?:boolean,
  last_name?:string
}
