import {CourseLevel} from "Contracts/constant";

export interface CourseDto {
  title: string
  description: string
  slug?: string
  is_pro?: boolean
  category_id: string
  is_premium?: boolean
  is_feature?: boolean
  price: number
  duration?: bigint
  is_live?: boolean
  tags: string
  to_learn: string
  level: CourseLevel
  url?: string
}


