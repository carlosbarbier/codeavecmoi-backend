interface IStudentCoursesBuilder {
  name: string;
  role: string;
  email: string;
  coursesCompleted: any;
  allCourses: any;
}

export class StudentCoursesBuilder {
  private readonly studentCoursesBuilder: IStudentCoursesBuilder;

  constructor() {
    this.studentCoursesBuilder = {
      name: '',
      role: '',
      email: '',
      coursesCompleted: [],
      allCourses: [],
    };
  }

  name(name: string): StudentCoursesBuilder {
    this.studentCoursesBuilder.name = name;
    return this;
  }

  role(role: string): StudentCoursesBuilder {
    this.studentCoursesBuilder.role = role;
    return this;
  }

  email(email: string): StudentCoursesBuilder {
    this.studentCoursesBuilder.email = email;
    return this;
  }

  coursesCompleted(coursesCompleted: any): StudentCoursesBuilder {
    this.studentCoursesBuilder.coursesCompleted = coursesCompleted;
    return this;
  }

  allCourses(allCourses: any): StudentCoursesBuilder {
    this.studentCoursesBuilder.allCourses = allCourses;
    return this;
  }

  build(): IStudentCoursesBuilder {
    return this.studentCoursesBuilder;
  }
}
