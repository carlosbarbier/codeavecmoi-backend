import Mail from "@ioc:Adonis/Addons/Mail";

interface IObserver {
  onChangeHappened(): void
}

export class CommentObserver {
  private observers: IObserver[];

  constructor() {
    this.observers = [];
  }

  addObserver(observer: IObserver) {
    this.observers.push(observer);
  }

  removeObserver(observer: IObserver) {
    const obsIndex = this.observers.indexOf(observer);
    if (obsIndex < 0) {
      return;
    }
    this.observers = this.observers.filter(obs => obs !== observer);
  }

 notifyTeacher() {
    this.observers.forEach( async observer => await observer.onChangeHappened());
  }
}

export class TeacherNotification implements IObserver {
  name: string;
  email: string;
  message: string;
  student_name: string

  constructor(name: string, email: string, message: string, student_name:string) {
    this.name = name;
    this.email = email;
    this.message = message;
    this.student_name = student_name
  }

  async onChangeHappened():  Promise<void> {
    await Mail.send((message) => {
      message
        .from('info@codeavecmoi.com')
        .to(this.email)
        .subject('comment for the course')
        .htmlView('comment', {name: this.name,message:this.message,student_name:this.student_name})
    })
  }
}
