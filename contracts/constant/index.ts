export enum SocialAccount {
  FACEBOOK = 'facebook',
  GITHUB = 'github',
  GOOGLE = 'google',
  EMAIL = 'email',

}
export enum UserRole {
  ADMIN = 'admin',
  TEACHER = 'teacher',
  STUDENT = 'student',

}

export enum CourseLevel {
  BEGINNER = 'Beginner',
  ALL = 'All level',
  ADVANCED = 'Advanced',
  INTERMEDIARY = 'Intermediary',

}

export enum Currency {
  EURO = 'eur',
  DOLLAR = 'usd',
  POUNDS = 'gbp',
}

export enum PaymentType {
  CARD = 'card',
  PAYPAL = 'paypal',
  OTHER = 'other',
}

export enum Status {
  CREATED = 'created',
  ACCEPTED = 'accepted',
  REJECTED = 'rejected',
  COMPLETED = 'completed',
}
