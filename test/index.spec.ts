import test from 'japa'
import supertest from 'supertest'
import {ErrorMessage} from "Contracts/message/ErrorMessage";
import User from "App/Models/User";
import Category from "App/Models/Category";
import {CourseLevel, UserRole} from "Contracts/constant";

const STUDENT_EMAIL = 'student@code.com'
const TEACHER_EMAIL = 'teacher@code.com'
const ADMIN_EMAIL = 'admin@code.com'
const TEACHER_NAME = 'karl'
const TEACHER_LAST_NAME = 'karl'
const INVALID_EMAIL = 'gmail.com'
const PASSWORD = '123456'
const INVALID_PASSWORD = '12345'
const EMAIL_NOT_SAVE = 'notsave@gmail.com'
const NAME = 'sudo'
let STUDENT_TOKEN: any;
let TEACHER_TOKEN: any;
let ADMIN_TOKEN: any;
let CATEGORY: any
const SLUG = "javascript"

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

/**
 * CATEGORY
 */
test.group('category', () => {
  test('save new category', async (assert) => {
    CATEGORY = await Category.create({name: "back", slug: "back"})
    assert.exists(CATEGORY.id)
  }).timeout(6000)
})

/*
TEACHER API
 */
test.group('teacher api', () => {

  test('register new teacher with invalid email and password', async (assert) => {
    const {text} = await supertest(BASE_URL)
      .post('/api/register/teacher')
      .send({name: NAME, email: INVALID_EMAIL, password: INVALID_PASSWORD})
      .expect(422)
    const json = JSON.parse(text)
    assert.isArray(json.errors)
  }).timeout(6000)

  test('register new teacher 200 success', async () => {
    await supertest(BASE_URL)
      .post('/api/register/teacher')
      .send({name: TEACHER_NAME, email: TEACHER_EMAIL, password: PASSWORD, last_name: TEACHER_LAST_NAME})
      .expect(200)
  }).timeout(6000)

  test('logging in teacher account not activate', async () => {
    await supertest(BASE_URL)
      .post('/api/login')
      .send({email: TEACHER_EMAIL, password: PASSWORD})
      .expect(400)
  }).timeout(6000)

  test('logging in teacher account activate', async (assert) => {
    await User.query().where("email", TEACHER_EMAIL).update({is_active: true})
    const {text} = await supertest(BASE_URL)
      .post('/api/login')
      .send({email: TEACHER_EMAIL, password: PASSWORD})
      .expect(200)
    const json = JSON.parse(text)
    TEACHER_TOKEN = json.token
    assert.exists(json.token)
    assert.exists(json.user)
  }).timeout(6000)

  test('forgot password teacher', async () => {
    await supertest(BASE_URL)
      .post('/api/password/forgot')
      .send({email: TEACHER_EMAIL, password: PASSWORD})
      .expect(200)
  }).timeout(6000)

  test('Change teacher Password', async () => {
    await supertest(BASE_URL)
      .put('/api/password/change')
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .send({password: PASSWORD, password_confirmation: PASSWORD, current_password: PASSWORD})
      .expect(200)
  }).timeout(6000)

  test('reset teacher password', async () => {
    const teacher = await User.query().where("email", TEACHER_EMAIL).first()
    await supertest(BASE_URL)
      .put(`/api/password/reset/${teacher?.token}`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .send({password: PASSWORD})
      .expect(200)
  }).timeout(6000)

  test('get teacher info', async () => {
    await supertest(BASE_URL)
      .get(`/api/users/verify`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .send({password: PASSWORD})
      .expect(200)
  }).timeout(6000)

  test('create new course', async () => {
    await supertest(BASE_URL)
      .post(`/api/teacher/courses`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .field('title', SLUG)
      .field('description', 'javascript course')
      .field('price', 0)
      .field('tags', 'js')
      .field('category_id', CATEGORY.id)
      .field('level', CourseLevel.ADVANCED)
      .field('to_learn', 'javascript')
      .attach('image', './tmp/images/js.jpg')
      .expect(201)

  }).timeout(15000)

  test('add video introduction', async () => {
    await supertest(BASE_URL)
      .post(`/api/teacher/courses/${SLUG}/videos`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .field('title', 'Introduction')
      .attach('video', './tmp/videos/video.mp4')
      .expect(200)
  }).timeout(20000)

  test('add video Conclusion', async () => {
    await supertest(BASE_URL)
      .post(`/api/teacher/courses/${SLUG}/videos`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .field('title', 'Conclusion')
      .attach('video', './tmp/videos/video2.mp4')
      .expect(200)
  }).timeout(20000)

  test('restricted route', async () => {
    await supertest(BASE_URL)
      .get(`/api/users/verify`)
      .set('Authorization', 'Bearer ' + "token")
      .send({password: PASSWORD})
      .expect(401)
  }).timeout(6000)

  test('get all courses', async () => {
    await supertest(BASE_URL)
      .get(`/api/teacher/courses`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .expect(200)
  }).timeout(2000)

  test('get all qas', async () => {
    await supertest(BASE_URL)
      .get(`/api/teacher/qas`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('get dashboard info', async () => {
    await supertest(BASE_URL)
      .get(`/api/teacher/dashboard`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('get dashboard info', async () => {
    await supertest(BASE_URL)
      .get(`/api/teacher/payments`)
      .set('Authorization', 'Bearer ' + TEACHER_TOKEN)
      .expect(200)
  }).timeout(6000)

})

/*
STUDENT API
 */
test.group('student api testing', () => {
  test('register new user with blank value', async (assert) => {
    const {text} = await supertest(BASE_URL)
      .post('/api/register')
      .send()
      .expect(422)
    const json = JSON.parse(text)
    assert.isArray(json.errors)
    assert.deepEqual(json.errors[0].field, 'email')
    assert.deepEqual(json.errors[1].field, 'name')
    assert.deepEqual(json.errors[2].field, 'password')
  }).timeout(6000)

  test('register new user with invalid email and password', async (assert) => {
    const {text} = await supertest(BASE_URL)
      .post('/api/register')
      .send({name: NAME, email: INVALID_EMAIL, password: INVALID_PASSWORD})
      .expect(422)
    const json = JSON.parse(text)
    assert.isArray(json.errors)
    assert.deepEqual(json.errors[0].field, 'email')
    assert.deepEqual(json.errors[0].message, 'invalid email')
    assert.deepEqual(json.errors[1].message, 'minimum password length 6 characters')
  }).timeout(6000)

  test('register new user 200 success', async () => {
    await supertest(BASE_URL)
      .post('/api/register')
      .send({name: NAME, email: STUDENT_EMAIL, password: PASSWORD})
      .expect(201)
  }).timeout(6000)

  test('logging in with invalid email', async (assert) => {
    const {text} = await supertest(BASE_URL)
      .post('/api/login')
      .send({email: INVALID_EMAIL, password: PASSWORD})
      .expect(422)
    const json = JSON.parse(text)
    assert.isArray(json.errors)
    assert.deepEqual(json.errors[0].field, 'email')
  }).timeout(6000)

  test('logging in with an email not register', async (assert) => {
    const {text} = await supertest(BASE_URL)
      .post('/api/login')
      .send({'email': EMAIL_NOT_SAVE, 'password': PASSWORD})
      .expect(400)
    const json = JSON.parse(text)
    assert.isArray(json.errors)
    assert.deepEqual(json.errors[0].message, ErrorMessage.USER_NOT_FOUND)
  }).timeout(6000)

  test('logging in studen account not activate', async () => {
    await supertest(BASE_URL)
      .post('/api/login')
      .send({email: STUDENT_EMAIL, password: PASSWORD})
      .expect(400)
  }).timeout(6000)

  test('logging in student account activate', async (assert) => {
    await User.query().where("email", STUDENT_EMAIL).update({is_active: true})
    const {text} = await supertest(BASE_URL)
      .post('/api/login')
      .send({email: STUDENT_EMAIL, password: PASSWORD})
      .expect(200)
    const json = JSON.parse(text)
    STUDENT_TOKEN = json.token
    assert.exists(json.token)
    assert.exists(json.user)
  }).timeout(6000)

  test('Change student Password', async () => {
    await supertest(BASE_URL)
      .put('/api/password/change')
      .set('Authorization', 'Bearer ' + STUDENT_TOKEN)
      .send({password: PASSWORD, password_confirmation: PASSWORD, current_password: PASSWORD})
      .expect(200)
  }).timeout(6000)

  test('student enrollment', async () => {
    await supertest(BASE_URL)
      .put(`/api/student/courses/${SLUG}/enroll`)
      .set('Authorization', 'Bearer ' + STUDENT_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('student enrollment failed if not authenticated', async () => {
    await supertest(BASE_URL)
      .put(`/api/student/courses/${SLUG}/enroll`)
      .set('Authorization', 'Bearer ' + "")
      .expect(401)
  }).timeout(6000)

  test('student watch course', async () => {
    await supertest(BASE_URL)
      .get(`/api/courses/${SLUG}/watch`)
      .set('Authorization', 'Bearer ' + STUDENT_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('student rate course', async () => {
    await supertest(BASE_URL)
      .put(`/api/student/courses/${SLUG}/rates`)
      .set('Authorization', 'Bearer ' + STUDENT_TOKEN)
      .send({rating: 5})
      .expect(200)
  }).timeout(6000)

  test('get student info', async () => {
    await supertest(BASE_URL)
      .get(`/api/users/verify`)
      .set('Authorization', 'Bearer ' + STUDENT_TOKEN)
      .send({password: PASSWORD})
      .expect(200)
  }).timeout(6000)

})

/*
PUBLIC API
 */
test.group('public api', () => {

  test('get all courses', async () => {
    await supertest(BASE_URL)
      .get(`/api/courses`)
      .expect(200)
  }).timeout(6000)

  test('get a  single course', async () => {
    await supertest(BASE_URL)
      .get(`/api/courses/${SLUG}`)
      .expect(200)
  }).timeout(6000)

  test('get all categories', async () => {
    await supertest(BASE_URL)
      .get(`/api/categories`)
      .expect(200)
  }).timeout(6000)
})



/*
ADMIN API
 */
test.group('admin api', () => {

  test('register admin 200 success', async () => {
    await supertest(BASE_URL)
      .post('/api/register/teacher')
      .send({name: TEACHER_NAME, email: ADMIN_EMAIL, password: PASSWORD, last_name: TEACHER_LAST_NAME})
      .expect(200)
  }).timeout(6000)

  test('logging in admin account', async (assert) => {
    await User.query().where("email", ADMIN_EMAIL).update({is_active: true,role:UserRole.ADMIN})
    const {text} = await supertest(BASE_URL)
      .post('/api/login')
      .send({email: ADMIN_EMAIL, password: PASSWORD})
      .expect(200)
    const json = JSON.parse(text)
    ADMIN_TOKEN = json.token
    assert.exists(json.token)
    assert.exists(json.user)
  }).timeout(6000)

  test('admin get all courses', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/courses`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('admin get a single course', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/courses/${SLUG}`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(2000)

  test('admin get dashboard info', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/dashboard`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('admin get all teachers', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/teachers`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('admin get app heath point', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/health`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(6000)

  test('get payments', async () => {
    await supertest(BASE_URL)
      .get(`/api/admin/payments`)
      .set('Authorization', 'Bearer ' + ADMIN_TOKEN)
      .expect(200)
  }).timeout(6000)
})

