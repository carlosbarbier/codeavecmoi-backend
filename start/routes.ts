import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

Route.group(() => {
  Route.group(() => {
    Route.get('/home', 'CourseController.coursesForHomePage')
    Route.post('/register', 'auth/RegisterController.register')
    Route.post('/register/teacher', 'auth/RegisterController.registerInstructor')
    Route.post('/login/google', 'auth/SocialAuthenticationController.loginOrRegisterWithGoogle')
    Route.post('/login/facebook', 'auth/SocialAuthenticationController.loginOrRegisterWithFacebook')
    Route.post('/login', 'auth/LoginController.login')
    Route.put('/account/activate/:token', 'auth/RegisterController.validateEmail')
    Route.post('/password/forgot', 'auth/ResetPasswordController.forgotPassword')
    Route.put('/password/reset/:token', 'auth/ResetPasswordController.resetPassword')
    Route.post('/logout', 'auth/LoginController.logout').middleware('auth')
    Route.put('/password/change', 'auth/ChangePasswordController.changePassword').middleware('auth')
    Route.get('/categories', 'CategoryController.index')
    Route.get('/courses', 'CourseController.index')
    Route.get('/courses-home', 'CourseController.coursesForHomePage')
    Route.get('/courses/:slug', 'CourseController.show')
    Route.get('/teacher/:name/courses', 'CourseController.coursesByTeacherName')
    Route.put('/account/update-profile', 'auth/UpdateProfileController.updateProfile').middleware("auth")
    Route.get('/users/verify', 'auth/LoginController.veryUser').middleware("auth")
  })

//TEACHER
  Route.group(() => {
    Route.post('/courses', 'teacher/CourseController.store')
    Route.get('/courses', 'teacher/CourseController.index')
    Route.post('/courses/:slug/videos', 'teacher/VideoController.store')
    Route.get('/courses/:slug/:videos?', 'teacher/CourseController.show')
      .where("videos", '^(true|false)$')
    Route.put('/courses/:slug', 'teacher/CourseController.edit')
    Route.put('/courses/:slug/completed', 'teacher/CourseController.completed')
    Route.put('/videos', 'teacher/VideoController.rearrangePositions')
    Route.delete('/videos/:id', 'teacher/VideoController.deleteVideo')
    Route.get('/qas', 'teacher/QaController.index')
    Route.post('/qas', 'teacher/QaController.store')
    Route.post('/cards', 'teacher/CardController.store')
    Route.put('/cards/:id', 'teacher/CardController.update')
    Route.delete('/cards/:id', 'teacher/CardController.delete')
    Route.put('/profile', 'teacher/ProfileController.store')
    Route.put('/update/profile', 'teacher/ProfileController.update')
    Route.put('/courses/:slug/activate', 'teacher/CourseController.activate')
    Route.get('/', 'auth/LoginController.veryUser')
    Route.get('/dashboard', 'teacher/DashboardController.index')
    Route.get('/payments', 'teacher/PaymentController.index')
  }).prefix('/teacher')
    .middleware('teacher')

  //STUDENT
  Route.group(() => {
    Route.put('/courses/:slug/enroll', 'student/CourseController.enrolForFreeCourse')
    Route.get('/courses', 'student/CourseController.index')
    Route.put('/courses/:slug/like', 'student/CourseController.like')
    Route.put('/courses/:slug/unlike', 'student/CourseController.unlike')
    Route.put('/courses/:slug/rates', 'student/CourseController.rating')
    Route.put('/courses/:slug/completed', 'student/CourseController.completed')
    Route.put('/courses/:slug/videos', 'student/VideoController.store')
    Route.post('/courses/:slug/questions-answers', 'student/QaController.store')
    Route.get('/courses/:slug/questions-answers', 'student/QaController.index')
    Route.put('/account/change-password', 'auth/ChangePasswordController.changePassword')
    Route.post('/charge', 'student/PaymentController.store')
    Route.get('/', 'auth/LoginController.veryUser')
  }).prefix('/student')
    .middleware('student')
  Route.group(() => {
    Route.get('/courses/:slug/watch', 'student/CourseController.show')
  })
    .middleware('student')

//ADMIN
  Route.group(() => {
    Route.get('/courses', 'admin/CourseController.index')
    Route.get('/courses/:slug', 'admin/CourseController.show')
    Route.put('/courses/:slug/approve', 'admin/CourseController.approve')
    Route.put('/courses/:slug/reject', 'admin/CourseController.reject')
    Route.post('/videos', 'admin/VideoController.store')
    Route.get('/payments', 'admin/PaymentController.index')
    Route.get('/qas', 'admin/QaController.index')
    Route.get('/', 'auth/LoginController.veryUser')
    Route.get('/dashboard', 'admin/DashboardController.index')
    Route.get('/teachers', 'admin/UserController.teachers')
    Route.get('/students', 'admin/DashboardController.students')
    Route.get('/health', async ({response}) => {
      const isLive = await HealthCheck.isLive()
      return isLive
        ? response.status(200).send(await HealthCheck.getReport())
        : response.status(400).send(await HealthCheck.getReport())
    })
  }).prefix('/admin')
  .middleware('admin')
  Route.get('/health', async ({response}) => {
    const isLive = await HealthCheck.isLive()
    return isLive
      ? response.status(200).send(await HealthCheck.getReport())
      : response.status(400).send(await HealthCheck.getReport())
  })
}).prefix('/api')


