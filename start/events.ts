import Event from '@ioc:Adonis/Core/Event'
import Application from "@ioc:Adonis/Core/Application";
import Logger from "@ioc:Adonis/Core/Logger";
import Database from "@ioc:Adonis/Lucid/Database";

Event.on('new:user', 'UserListener.handleRegistration')
Event.on('forgot:password', 'UserListener.handlePasswordForgot')
Event.on('db:query', (query) => {
  if (Application.inProduction) {
    // @ts-ignore
    Logger.debug(query)
  } else {
    Database.prettyPrint(query)
  }
})
