import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Category from 'App/Models/Category'
import slugify from 'slugify'
import Redis from "@ioc:Adonis/Addons/Redis";

export default class CategorySeeder extends BaseSeeder {
  private data=[
    {
      'name':'Backend',
      'slug':slugify('Backend', { lower: true,replacement: '-'}),
    },
    {
      name:'Frontend',
      'slug':slugify('Frontend', { lower: true,replacement: '-'}),
    },
    {
      name:'Database',
      'slug':slugify('Database', { lower: true,replacement: '-'}),
    },
    {
      name:'Marchine Learning',
      'slug':slugify('some string', { lower: true,replacement: '-'}),
    },
    {
      name:'Seo',
      'slug':slugify('Marchine Learning', { lower: true,replacement: '-'}),
    },
    {
      name:'Graphic Design',
      'slug':slugify('Seo', { lower: true,replacement: '-'}),
    },
    {
      name:'UX UI Design',
      'slug':slugify('UX UI Design', { lower: true,replacement: '-'}),
    },
    {
      name:'Algorithm and Data structure',
      'slug':slugify('Algorithm and Data structure', { lower: true,replacement: '-'}),
    },
  ]
  public async run () {
    const categories=await Category.createMany(this.data)
    await Redis.set('categories', JSON.stringify(categories))
  }
}
