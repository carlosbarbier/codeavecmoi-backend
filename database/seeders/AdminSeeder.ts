import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from "App/Models/User";
import {UserRole} from "Contracts/constant";

export default class AdminSeeder extends BaseSeeder {

  public async run() {
    await User.create({
      name: "Barbier Kouakou",
      email: "kouakoubarbier@gmail.com",
      password: "Pa88word",
      role: UserRole.ADMIN,
      isActive: true,
    })

  }
}
