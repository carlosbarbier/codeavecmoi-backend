// import Factory from '@ioc:Adonis/Lucid/Factory'
import Factory from '@ioc:Adonis/Lucid/Factory'
import Course from 'App/Models/Course'
import slugify from "slugify";
import TeacherCourse from "App/Models/TeacherCourse";
import User from "App/Models/User";
import Video from "App/Models/Video";

export const TeacherCourseFactory = Factory
  .define(TeacherCourse, ({faker}) => {
    return {
      userId: 19,
      totalStudent: faker.random.number()
    }
  })
  .build()

export const UserFactory = Factory
  .define(User, ({ faker }) => {
    return {
      name: faker.name.firstName(),
      email: faker.internet.email(),
      password: "123456",
    }
  }).relation("teacher_courses",() => CourseFactory)
  .build()

export const VideoFactory = Factory
  .define(Video, ({faker}) => {
    return {
      title:faker.lorem.lines(1),
      url: "https://codeavecmoi.s3.eu-west-1.amazonaws.com/videos/1611856156108_videoone.mp4",
      duration:200
    }
  })
  .build()

export const CourseFactory = Factory
  .define(Course, ({faker}) => {
    const title = faker.lorem.lines(1)
    return {
      title,
      description: faker.lorem.sentence(24),
      categoryId: 12,
      url: "https://codeavecmoi.s3.amazonaws.com/images/1612021821577_AK-logo-circle2.png",
      tags: "html",
      slug: slugify(title, {lower: true, replacement: '-'})
    }
  }).relation("videos",() => VideoFactory)
  .build()


