import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Videos extends BaseSchema {
  protected tableName = 'videos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true)
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.string("url");
      table.string("title");
      table.string("duration");
      table.integer("position").defaultTo(0)
      table.boolean("is_completed").defaultTo(false);
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
