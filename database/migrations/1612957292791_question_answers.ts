import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class QuestionAnswers extends BaseSchema {
  protected tableName = 'question_answers'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("user_id").unsigned().references("id").inTable("users");
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.integer("parent_id").unsigned().references("id").inTable("question_answers").nullable().onDelete('cascade')
      table.text("body").notNullable();
      table.index(["parent_id"], "qa_index");
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}

