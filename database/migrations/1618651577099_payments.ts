import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import {PaymentType} from "Contracts/constant";

export default class Payments extends BaseSchema {
  protected tableName = 'payments'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("student_id").unsigned().references("id").inTable("users");
      table.integer("teacher_id").unsigned().references("id").inTable("users");
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.string('stripe_id')
      table.string('amount')
      table.string('receipt_url')
      table.boolean('is_refunded').defaultTo(false)
      table.enu('payment_type', Object.values(PaymentType)).defaultTo(PaymentType.CARD)
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
