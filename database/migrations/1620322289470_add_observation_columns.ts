import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AdminVideos extends BaseSchema {
  protected tableName = 'admin_videos'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string("observation");
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('observation')
    })
  }
}
