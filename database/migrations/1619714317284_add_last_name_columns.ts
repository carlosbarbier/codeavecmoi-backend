import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string("last_name")
      table.string("about")
      table.string("picture")
      table.string("facebook")
      table.string("website")
      table.string("youtube")
      table.string("twitter")
      table.string("instagram")
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("last_name")
      table.dropColumn('about')
      table.dropColumn("picture")
      table.dropColumn("facebook")
      table.dropColumn("website")
      table.dropColumn("youtube")
      table.dropColumn("twitter")
      table.dropColumn("instagram")
    })
  }
}
