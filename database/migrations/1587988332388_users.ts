import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import {UserRole,SocialAccount} from 'Contracts/constant'

export default class UsersSchema extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('name', 255).notNullable()
      table.enu('role', Object.values(UserRole)).defaultTo(UserRole.STUDENT)
      table.string('email', 255).notNullable().unique()
      table.string('password', 180).notNullable()
      table.string('token').nullable()
      table.boolean('is_active').defaultTo(false)
      table.enu('source',Object.values(SocialAccount)).defaultTo(SocialAccount.EMAIL)
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
