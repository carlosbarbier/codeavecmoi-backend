import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Cards extends BaseSchema {
  protected tableName = 'cards'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('cvc')
      table.string('number')
      table.string('expiry')
      table.string('name')
      table.string('stripe_id')
      table.integer(" teacher_id").unsigned().references("id").inTable("users");
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
