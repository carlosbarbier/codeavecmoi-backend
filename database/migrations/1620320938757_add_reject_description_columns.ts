import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
     this.schema.table(this.tableName, (table) => {
      table.string("reject_description")
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('reject_description')
    })
  }
}
