import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StudentCourses extends BaseSchema {
  protected tableName = 'student_courses'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer("student_id").unsigned().references("id").inTable("users");
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.boolean("is_completed").defaultTo(false);
      table.boolean("wishlist").defaultTo(false);
      table.boolean("is_archived").defaultTo(false);
      table.float("progress").defaultTo(0.00);
      table.boolean('is_paid').defaultTo(false)
      table.boolean('is_active').defaultTo(true)
      table.integer("rating").defaultTo(0);
      table.text("comment_body");
      table.text("comment_date");
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
