import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StudentCourses extends BaseSchema {
  protected tableName = 'student_courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string("author")
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('author')
    })
  }
}
