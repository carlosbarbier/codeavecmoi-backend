import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import {CourseLevel} from "Contracts/constant";

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments();
      table.string("title").notNullable();
      table.string("description").notNullable();
      table.string("slug").notNullable().unique();
      table.boolean("is_pro").defaultTo(false);
      table.integer("category_id")
        .unsigned()
        .references("id")
        .inTable("categories")
        .onDelete("cascade");
      table.boolean("is_premium").defaultTo(false);
      table.boolean("is_feature").defaultTo(false);
      table.double("price").defaultTo(0);
      table.bigInteger("duration").defaultTo(0.0);
      table.boolean("is_live").defaultTo(false)
      table.string("tags").notNullable()
      table.enu('level',Object.values(CourseLevel)).defaultTo(CourseLevel.ALL)
      table.string("url").defaultTo(null);
      table.text("to_learn")
      table.bigInteger("rating_total_sum").defaultTo(0.0)
      table.integer("rating").defaultTo(0)
      table.string("teacher_name")
      table.integer("total_video").defaultTo(0);
      table.timestamps();
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
