import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string("teacher_name_slug")
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("teacher_name_slug")
    })
  }
}
