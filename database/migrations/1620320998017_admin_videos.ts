import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AdminVideos extends BaseSchema {
  protected tableName = 'admin_videos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.integer("video_id").unsigned().references("id").inTable("videos");
      table.boolean("is_completed").defaultTo(true);
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
