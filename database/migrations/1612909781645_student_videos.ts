import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StudentVideos extends BaseSchema {
  protected tableName = 'student_videos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer("student_id").unsigned().references("id").inTable("users");
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.integer("video_id").unsigned().references("id").inTable("videos");
      table.boolean("is_completed").defaultTo(true);
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
