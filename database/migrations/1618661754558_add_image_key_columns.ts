import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string('image_key')
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('image_key')
    })
  }
}
