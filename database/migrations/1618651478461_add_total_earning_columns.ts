import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TeacherCourses extends BaseSchema {
  protected tableName = 'teacher_courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.double("total_earning").defaultTo(0)
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("total_earning",)
    })
  }
}
