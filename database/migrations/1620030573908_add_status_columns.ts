import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import {Status} from "Contracts/constant";

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.enu('status', Object.values(Status)).defaultTo(Status.CREATED)
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('status')
    })
  }
}
