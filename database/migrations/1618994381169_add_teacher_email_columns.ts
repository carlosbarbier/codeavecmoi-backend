import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up () {
    this.schema.table(this.tableName, (table) => {
      table.string("teacher_email")
      table.integer(" teacher_id");
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("teacher_email")
      table.dropColumn("teacher_id")
    })
  }
}
