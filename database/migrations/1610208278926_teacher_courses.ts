import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TeacherCourses extends BaseSchema {
  protected tableName = 'teacher_courses'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer("teacher_id").unsigned().references("id").inTable("users");
      table.integer("course_id").unsigned().references("id").inTable("courses");
      table.integer("total_student").defaultTo(0);
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
