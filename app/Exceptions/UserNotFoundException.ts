import { Exception } from '@poppinss/utils'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {ErrorMessage} from 'Contracts/message/ErrorMessage'
import {ErrorCode} from 'Contracts/error/ErrorCode'
import {CustomError} from 'Contracts/error/CustomError'

export default class UserNotFoundException extends Exception {
  constructor (message: any) {
    super(message, 404)
  }

  public async handle (error: this, { response }: HttpContextContract) {
    return response.send(CustomError.builder(ErrorCode.USER_NOT_FOUND_CODE||error.code, ErrorMessage.USER_NOT_FOUND))
  }
}
