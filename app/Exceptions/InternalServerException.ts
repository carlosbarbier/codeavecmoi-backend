import { Exception } from '@poppinss/utils'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {ErrorMessage} from 'Contracts/message/ErrorMessage'
import {ErrorCode} from 'Contracts/error/ErrorCode'
import {CustomError} from 'Contracts/error/CustomError'

export default class InternalServerException extends Exception {
  constructor (message: string) {
    super(message, 500)
  }

  // @ts-ignore
  public async handle (error: this, { response }: HttpContextContract) {
    return response.send(CustomError.builder(ErrorCode.SERVER_ERROR, ErrorMessage.INTERNAL_ERROR))
  }
}
