import Logger from '@ioc:Adonis/Core/Logger'
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler'
import {ErrorCode} from 'Contracts/error/ErrorCode'
import {CustomError} from 'Contracts/error/CustomError'
import {ErrorMessage} from 'Contracts/message/ErrorMessage'

export default class ExceptionHandler extends HttpExceptionHandler {
  constructor() {
    super(Logger)
  }

  public async handle(error, ctx) {
    if (error.code === 'E_INVALID_API_TOKEN') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.INVALID_TOKEN, ErrorMessage.INVALID_TOKEN))
    } else if (error.code === 'E_UNAUTHORIZED_ACCESS') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.INVALID_TOKEN, ErrorMessage.INVALID_TOKEN))
    } else if (error.code === 'E_ROUTE_NOT_FOUND') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.ROUTE_NOT_FOUND, ErrorMessage.ROUTE_NOT_FOUND))
    } else if (error.code === 'E_UNMANAGED_DB_CONNECTION') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.SERVER_ERROR, ErrorMessage.INTERNAL_ERROR))
    } else if (error.code === 'ECONNREFUSED') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.SERVER_ERROR, ErrorMessage.INTERNAL_ERROR))
    }else if (error.code === 'ER_DUP_ENTRY') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.SERVER_ERROR, ErrorMessage.INTERNAL_ERROR))
    }else if (error.code === 'E_RUNTIME_EXCEPTION') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.SERVER_ERROR, ErrorMessage.INTERNAL_ERROR))
    }else if (error.code === 'E_ROW_NOT_FOUND') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.NOT_FOUND, ErrorMessage.NOT_FOUND))
    }else if (error.code === 'E_INVALID_AUTH_UID') {
      return ctx.response
        .send(CustomError.builder(ErrorCode.USER_NOT_FOUND_CODE, ErrorMessage.USER_NOT_FOUND))
    }
    return super.handle(error, ctx)
  }
}
