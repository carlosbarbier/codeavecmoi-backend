import { Exception } from '@poppinss/utils'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {ErrorMessage} from 'Contracts/message/ErrorMessage'
import {ErrorCode} from 'Contracts/error/ErrorCode'
import {CustomError} from 'Contracts/error/CustomError'

export default class DataNotFoundException extends Exception {
  constructor (message: any) {
    super(message, 400)
  }

  public async handle (error: this, { response }: HttpContextContract) {
    return response.send(CustomError.builder(ErrorCode.NOT_FOUND||error.code, ErrorMessage.NOT_FOUND))
  }
}
