import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UnauthorizedException from 'App/Exceptions/UnauthorizedException'
import {UserRole} from 'Contracts/constant'

export default class Teacher {
  public async handle ({auth}: HttpContextContract, next: () => Promise<void>) {
    const user = await auth.use('api').authenticate()
    if (user && user.isActive && user.role === UserRole.TEACHER) {
      await next()
    } else {
      throw new UnauthorizedException('')
    }
  }
}
