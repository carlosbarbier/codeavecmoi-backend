import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UnauthorizedException from 'App/Exceptions/UnauthorizedException'
import {UserRole} from 'Contracts/constant'

export default class Student {
  public async handle ({auth}: HttpContextContract, next: () => Promise<void>) {
    const user = await auth.use('api').authenticate()
    console.log(user.isActive)
    if (user && user.isActive && user.role === UserRole.STUDENT) {
      await next()
    } else {
      throw new UnauthorizedException('')
    }
  }
}
