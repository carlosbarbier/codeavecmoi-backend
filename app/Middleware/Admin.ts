import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UnauthorizedException from 'App/Exceptions/UnauthorizedException'
import {UserRole} from 'Contracts/constant'

export default class Admin {
  public async handle ({auth}: HttpContextContract, next: () => Promise<void>) {
    const user = await auth.use('api').authenticate()
    if (user && user.isActive && user.role === UserRole.ADMIN) {
      await next()
    } else {
      throw new UnauthorizedException('')
    }
  }
}
