import Env from "@ioc:Adonis/Core/Env";
import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import {CardDto} from "Contracts/dto/CardDto";
import {ChargeDto} from "Contracts/dto/ChargeDto";
import {Currency} from "Contracts/constant";
import {CourseService} from "App/service/student/CourseService";
import {CreditCardDto} from "Contracts/dto/CreditCardDto";
import {StripeCardDto} from "Contracts/dto/StripeCardDto";

const stripe = require('stripe')(Env.get('STRIPE_SECRET_KEY'));

export class StripeService {

  static async proceedPayment(dto: CardDto, user: any) {
      const token = await this.generateToken(dto)
      if (token) {
        const chargeDto: ChargeDto = {amount: dto.amount, currency: Currency.EURO, description: "Code purchase"}
        const paymentData = await this.chargeCustomer(user, chargeDto, token.id)
        if (paymentData) {
          await CourseService.enrollForCourse(user, dto.slugs, paymentData)
        }else{
          throw new InternalServerException("")
        }
      }
  }

  private static async generateToken(dto: StripeCardDto) {
    try {
      return await stripe.tokens.create({
        card: {
          number: dto.number,
          exp_month: dto.exp_month,
          exp_year: dto.exp_year,
          cvc: dto.cvc,
        },
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  public static async getBalance() {
    try {
      return await stripe.balance.retrieve()
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }


  public static async transfer(amount: number) {
    try {
      return await stripe.transfers.create({
        amount: amount,
        currency: Currency.EURO,
        destination: '{{CONNECTED_STRIPE_ACCOUNT_ID}}',
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  public static async connectAccount() {
    try {
      return await stripe.accounts.create({
        type: 'standard',
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  public static async registerTeacher(user: any, token: any) {
    try {
      return await stripe.customers.create({
        email: user.email,
        source: token.id,
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  private static async chargeCustomer(user: any, dto: ChargeDto, source_id: string) {
    try {
      return await stripe.charges.create({
        amount: dto.amount * 100,
        description: dto.description,
        currency: dto.currency,
        source: source_id,
        receipt_email: user.email,
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  static async registerTeacherCardInfo(dto: CreditCardDto, teacher: any) {
    try {
      const exp_month = dto.expiry.slice(0, 2)
      const exp_year = "20" + dto.expiry.slice(-2)
      const token = await this.generateToken({number: dto.number, cvc: dto.cvc, exp_month, exp_year})
      return await this.registerTeacher(teacher, token)
    } catch (error) {
      Logger.error(`[-]${error.message}`);
      throw new InternalServerException("")
    }
  }

}


