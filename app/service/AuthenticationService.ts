import Logger from '@ioc:Adonis/Core/Logger'
import User from 'App/Models/User'
import {RegistrationDto} from 'Contracts/dto/RegistrationDto'
import InternalServerException from 'App/Exceptions/InternalServerException'
import {LoginDto} from 'Contracts/dto/LoginDto'
import {AuthContract} from '@ioc:Adonis/Addons/Auth'
import UserNotFoundException from 'App/Exceptions/UserNotFoundException'
import Event from '@ioc:Adonis/Core/Event'
import {v4 as uuidv4} from 'uuid'
import {SocialAccount, UserRole} from "Contracts/constant";
import {CustomAlert} from "Contracts/success/CustomAlert";
import {AlertMessage} from "Contracts/message/AlertMessage";
import {UpdateProfileDto} from "Contracts/dto/UpdateProfileDto";
import {ChangePasswordDto} from "Contracts/dto/ChangePasswordDto";
import Hash from '@ioc:Adonis/Core/Hash'
import AccountInactiveException from "../Exceptions/AccountInactiveException";
import slugify from "slugify";

export class AuthenticationService {
  private static message: String = 'inside user authentication service: '

  /**
   * register a user
   * send an email to the user to activate his account
   * @param dto:Registration
   */
  public static async register(dto: RegistrationDto) {
    Logger.debug(this.message + 'registering a new account')
    try {
      dto.token = uuidv4()
      const user = await User.create(dto)
      if (user) {
        await Event.emit('new:user', {user})
        return CustomAlert.builder(AlertMessage.EMAIL_ACTIVATION)
      }

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException('')
    }
  }

  /**
   * register a user
   * send an email to the user to activate his account
   * @param dto:Registration
   */
  public static async registerTeacher(dto: RegistrationDto) {
    Logger.debug(this.message + 'registering instructor')
    try {
      dto.token = uuidv4()
      const name_slug = slugify(dto.name, {lower: true, replacement: '-'})
      // @ts-ignore
      const last_name_slug = slugify(dto.last_name, {lower: true, replacement: '-'})
      const user = await User.create({...dto, role: UserRole.TEACHER, nameSlug: name_slug + "-" + last_name_slug})
      await Event.emit('new:user', {user})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException('')
    }
  }

  /**
   * user login
   * @param email
   * @param password
   * @param auth
   */
  public static async login({email, password}: LoginDto, auth: AuthContract) {
    Logger.debug(this.message + 'logging in to account')
    const {user, token} = await auth.use('api').attempt(email, password)
    if (user.isActive) {
      return Object.assign({token: token}, {user: user.toJSON()})
    } else {
      throw new AccountInactiveException('')
    }
  }

  /**
   * user sign out
   * @param auth
   */
  public static async logout(auth: AuthContract) {
    Logger.debug(this.message + 'logging out to account')
    try {
      await auth.use('api').logout()
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException('')
    }
  }

  /**
   * activate account
   * @param token
   */
  public static async activateAccount(token: string) {
    Logger.debug(this.message + 'activating account')
    try {
      console.log("LOG",token)
       await User.query().where('token', token).update({is_active: true, token: null})

    } catch (e) {
      Logger.error(e.message)
      throw new UserNotFoundException('')
    }
  }

  public static async sendEmailToUserToResetPassword(email: string) {
    const user = await this.getUserBy('email', email)
    if (user) {
      try {
        user.token = uuidv4()
        const usersSave = await user.save()
        if (usersSave) {
          await Event.emit('forgot:password', {user})
        }
      } catch (e) {
        Logger.error(e.message)
        throw new InternalServerException('')
      }
    }
  }

  public static async resetPassword(password: string, token: string) {
    const user = await this.getUserBy('token', token)
    if (user) {
      try {
        user.password = password
        await user.save()
      } catch (e) {
        Logger.error(e.message)
        throw new InternalServerException('')
      }
    }
  }

  public static async changePassword(dto: ChangePasswordDto, user: any) {
    try {
      const hasSameValue = await Hash.verify(user.password, dto.current_password)
      if (hasSameValue) {
        user.password = dto.password
        await user.save()
      } else {
        // need to be updated
        throw new InternalServerException('')
      }

    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException('')
    }
  }

  public static async verifyUser(auth: AuthContract) {
    try {
      return await auth.use('api').authenticate()
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException('')
    }
  }

  public static async changeProfile(profile: UpdateProfileDto, user: any) {
    try {
      user.name = profile.name
      user.email = profile.email
      await user.save()
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException('')
    }
  }

  public static async socialLogin(dto: RegistrationDto, source: SocialAccount, auth: AuthContract) {
    Logger.debug(this.message + 'logging with ' + source + ' account')
    try {

      const {email, name, password, role} = dto
      const user = await User.findBy('email', email)
      if (user) {
        const {token} = await auth.use('api').generate(user)
        return Object.assign({token: token}, {user: user.toJSON()})
      } else {
        const newUser = await User.create({email, password, isActive: true, name, role, source})
        const {token} = await auth.use('api').generate(newUser)
        return Object.assign({token: token}, {user: newUser.toJSON()})
      }

    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException('')
    }
  }

  private static async getUserBy(key: string, value: string) {
    try {
      return await User.query().where(key, value).first()
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException('')
    }
  }
}
