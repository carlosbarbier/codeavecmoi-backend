import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import Course from "App/Models/Course";
import User from "App/Models/User";
import TeacherCourse from "App/Models/TeacherCourse";

export class CourseService {
  private static message: string = 'inside course service: '

  /**
   * getAllMyCourses all courses
   */
  public static async getAllCourses(filter: any, page: number) {
    Logger.info(this.message + "getting all courses")
    try {
      if (Object.keys(filter).length === 0) {
        return await Course.query().where("is_live",true).paginate(page, 5)
      } else {
        return await Course.filter(filter).where("is_live",true).paginate(page, 5)
      }
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * getAllMyCourses course by id or slug
   */
  public static async getCourseBySlug(slug: string) {
    Logger.info(this.message + "getting course  slug")
    try {
      return await Course.query()
        .where('slug', slug)
        .preload('category')
        .preload("students")
        .preload('videos').firstOrFail()
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * get course for homepage
   */
  public static async getCoursesForHomePage() {
    Logger.info(this.message + "getting course  slug")
    try {
      const premium = await Course.query()
        .where("is_premium", true)
        .where("is_live",true)
        .preload('category')
        .limit(4)
      const latest = await Course.query()
        .whereNot("is_premium", true)
        .where("is_live",true)
        .preload('category')
        .orderBy("created_at", "desc")
        .limit(4)
      const courses = await Course.query().preload('category').limit(100)

      return Object.assign({
        premium_courses: premium,
        latest_courses: latest,
        courses,
      })

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * get  all courses without pagination
   */
  public static async getAllCoursesWithoutPagination() {
    Logger.info(this.message + "getting all courses without pagination")
    try {
      return await Course.query().where("is_live",true).preload('category').limit(100)
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * get  all courses without pagination
   */
  public static async getCourseByTeacherName(name: string) {
    Logger.info(this.message + "getting courses by teacher name")
    try {
      const courses = await Course.query().where("is_live",true).where("teacher_name_slug", name)
      if (courses.length > 0) {
        const teacher = await User.query().where("id", courses[0].teacherId).firstOrFail()
        const total_student = await TeacherCourse.query()
          .where("teacher_id", teacher.id)
          .sum("total_student as total_student ")
          .first()
        return Object.assign({
          profile: teacher,
          courses: courses,
          total_student,
        })
      }
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }
}
