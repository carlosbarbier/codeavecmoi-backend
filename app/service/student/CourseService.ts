import Course from "App/Models/Course";
import InternalServerException from "App/Exceptions/InternalServerException";
import Logger from "@ioc:Adonis/Core/Logger";
import StudentCourse from "App/Models/StudentCourse";
import User from "App/Models/User";
import DataNotFoundException from "App/Exceptions/DataNotFoundException";
import {RatingDto} from "Contracts/dto/RatingDto";
import {DateTimeUtil} from "../../../util/DateTimeUtil";
import {StudentCoursesBuilder} from "Contracts/pattern/builder/builder";
import TeacherCourse from "App/Models/TeacherCourse";
import Payment from "App/Models/Payment";

export class CourseService {

  public static async enrolForFreeCourse(slug: string, user: any) {
    const course = await this.findCourse(slug);
    const payload = {courseId: course.id, studentId: user.id, author: user.name}
    try {
      await StudentCourse.firstOrCreate(payload, payload)
      await this.teacherTotalStudent(course.id, course.teacherId)
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async likeCourse(slug: string, user: any) {
    const course = await Course.findByOrFail("slug", slug);
    try {
      await StudentCourse.query()
        .where("course_id", course.id)
        .andWhere("student_id", user.id)
        .update({wishlist: true});
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async unlikeCourse(slug: string, user: any) {
    const course = await Course.findByOrFail("slug", slug);
    try {
      await StudentCourse.query().where("course_id", course.id).andWhere("student_id", user.id).update({wishlist: false});
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getMyCourses(user: any) {
    try {
      const all_courses = await this.getAllEnrolledCourses(user)
      const completed_courses = await this.getAllCompletedCourses(user)
      return new StudentCoursesBuilder()
        .name(user.name)
        .role(user.role)
        .email(user.email)
        .allCourses(all_courses.student_courses)
        .coursesCompleted(completed_courses.student_courses)
        .build()

    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getAllMyCoursesCompleted(user: any) {
    try {
      const courses = await User
        .query()
        .where('id', user.id)
        .preload('student_courses', (query) => {
          query.wherePivot('student_id', user.id)
          query.wherePivot('is_completed', true)
        })
      return courses[0]
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async rateCourse(user: any, slug: string, ratingDto: RatingDto) {
    try {
      const course = await Course.findByOrFail("slug", slug);
      if (ratingDto.comment_body) {
        await StudentCourse.query()
          .where("course_id", course.id)
          .andWhere("student_id", user.id)
          .update({rating: ratingDto.rating, comment_body: ratingDto.comment_body, comment_date: DateTimeUtil.now()});
      } else {
        await StudentCourse.query()
          .where("course_id", course.id)
          .andWhere("student_id", user.id)
          .update({rating: ratingDto.rating, comment_body: ratingDto.comment_body});
      }
      await Course.query()
        .where("id", course.id)
        .increment('rating', ratingDto.rating)
        .increment('rating_total_sum', 1)
    } catch (e) {
      if (e.code === "E_ROW_NOT_FOUND") {
        throw  new DataNotFoundException("")
      }
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async markCourseAsCompleted(user: any, slug: string) {
    try {
      const course = await Course.findByOrFail("slug", slug);
      await StudentCourse.query().where("course_id", course.id).andWhere("student_id", user.id).update({is_completed: true});
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  /**
   * watch course
   */
  public static async watchCourse(slug: string, user) {
    try {
      return await Course.query().where("slug", slug)
        .preload('students', (builder => {
          builder.where("student_id", user.id)
        }))
        .preload('videos')
        .firstOrFail()
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  private static async getAllEnrolledCourses(user: any) {
    const courses = await User
      .query()
      .where('id', user.id)
      .preload('student_courses', (query) => {
        query.wherePivot('student_id', user.id)
      })
    return courses[0]
  }

  private static async getAllCompletedCourses(user: any) {
    const courses = await User
      .query()
      .where('id', user.id)
      .preload('student_courses', (query) => {
        query.wherePivot('student_id', user.id)
        query.wherePivot('is_completed', true)
      })
    return courses[0]
  }

  public static async enrollForCourse(user: any, slugs: Array<string>, paymentData: any) {
    for (let i = 0; i < slugs.length; i++) {
      const course = await this.findCourse(slugs[i]);
      await this.saveStudentCourse(course.id, user.id)
      await this.savePayment(course.id, user.id, course.teacherId, course.price, paymentData);
      await this.teacherEarnings(course.id, course.teacherId, course.price)
    }
  }

  private static async teacherEarnings(courseId: number, teacherId: number, amount: number) {
    try {
      return await TeacherCourse.query()
        .where("course_id", courseId)
        .where("teacher_id", teacherId)
        .increment('total_earning', amount)
        .increment('total_student', 1)

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }

  private static async teacherTotalStudent(courseId: number, teacherId: number) {
    try {
      return await TeacherCourse.query()
        .where("course_id", courseId)
        .where("teacher_id", teacherId)
        .increment('total_student', 1)

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }

  public static async getTotalRating(ids: Array<number>) {
    try {
      return await StudentCourse.query()
        .where("rating", ">", 0)
        .andWhereIn("course_id", ids)
        .count('* as rating')
        .first()

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }

  public static async getTotalNewStudent() {
    try {
      return await StudentCourse.query()
        .where("created_at", ">", DateTimeUtil.yesterday())
        .count('* as new_student')
        .first()

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }

  private static async findCourse(slug: string) {
    try {
      return await Course.findByOrFail("slug", slug);
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  private static async savePayment(courseId: number, studentId: number, teacherId: number, amount: number, paymentData: any) {
    try {
      await Payment.create({
        courseId,
        studentId,
        teacherId,
        stripeId: paymentData.id,
        amount: amount,
        receiptUrl: paymentData.receipt_url,
        isPaid: paymentData.paid,
        status: paymentData.status,
      });
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }

  private static async saveStudentCourse(courseId: number, studentId: number) {
    try {
      const studentCourse = new StudentCourse()
      studentCourse.courseId = courseId
      studentCourse.studentId = studentId
      await studentCourse.save()
    } catch (error) {
      Logger.error(`[-]${error}`);
      throw new InternalServerException("")
    }
  }
}



