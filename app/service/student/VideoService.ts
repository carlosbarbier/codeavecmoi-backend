import Course from "App/Models/Course";
import InternalServerException from "App/Exceptions/InternalServerException";
import Logger from "@ioc:Adonis/Core/Logger";
import StudentVideo from "App/Models/StudentVideo";
import Video from "App/Models/Video";
import StudentCourse from "App/Models/StudentCourse";

export class VideoService {

  public static async finishToWatchVideo(slug: string, user: any, video_id: number) {
    try {
      const course = await Course.findByOrFail("slug", slug);
      const video = await Video.query().where("id", video_id).andWhere("course_id", course.id).firstOrFail();
      await StudentVideo.create({courseId: course.id, studentId: user.id, videoId: video.id})
      await StudentCourse
        .query()
        .where("course_id", course.id)
        .andWhere("student_id", user.id)
        .increment('progress', video.duration);
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getVideoAlreadyWatchIds(course_id: number, user: any) {
    try {
      return await StudentVideo.query().where("student_id", user.id).where("course_id", course_id)
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }
}



