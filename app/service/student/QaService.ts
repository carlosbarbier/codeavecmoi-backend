import Course from "App/Models/Course";
import InternalServerException from "App/Exceptions/InternalServerException";
import Logger from "@ioc:Adonis/Core/Logger";
import QuestionAnswer from "App/Models/QuestionAnswer";
import {QaDto} from "Contracts/dto/QaDto";
import {CommentObserver, TeacherNotification} from "Contracts/pattern/observer/observer";

export class QaService {

  public static async saveQuestionAnswer(slug: string, qaDto: QaDto, user: any) {
    try {
      const course = await Course.findByOrFail("slug", slug);
      const questionAnswer = await QuestionAnswer.create({
        courseId: course.id,
        userId: user.id,
        body: qaDto.body,
        parentId: qaDto.parent_id,
      });
      await this.notifyTeacher(course.teacherName, course.teacherEmail, questionAnswer.body, user.name)
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getAllQuestionsAndReplies(slug: string) {
    try {
      const course = await Course.findByOrFail("slug", slug);
      return await QuestionAnswer.query().where('course_id', course.id)
        .preload('user')
        .preload('replies', (builder => {
          builder.preload('user')
        })).whereNull('parent_id');
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  private static async notifyTeacher(name: string, email: string, message: string, student_name: string) {
    try {
      const notification = new TeacherNotification(name, email, message, student_name)
      let observer = new CommentObserver();
      observer.addObserver(notification)
      observer.notifyTeacher()
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }
}



