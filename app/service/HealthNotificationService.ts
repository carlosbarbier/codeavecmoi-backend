import HealthCheck from "@ioc:Adonis/Core/HealthCheck";
import {CronJob} from "cron";
import Mail from "@ioc:Adonis/Addons/Mail";



export class HealthNotificationService {
  private static instance: HealthNotificationService;
  private constructor() { }

  public static getInstance(): HealthNotificationService {
    if (!HealthNotificationService.instance) {
      HealthNotificationService.instance = new HealthNotificationService();
    }
    return HealthNotificationService.instance;
  }

  public runJob() {
     const job=new CronJob('*/10 * * * *', async () => {
       console.log("RUNNING")
      try {
        const isLive = await HealthCheck.isLive()
        if(isLive){
          const report=await HealthCheck.getReport()
          await Mail.send((message) => {
            message
              .from('info@codeavecmoi.com')
              .to("kouakoubarbier@gmail.com")
              .subject('comment for the course')
              .htmlView('report', {report:JSON.stringify(report)})
          })
        }
      } catch (e) {
        console.error(e);
      }
    })
    if (!job.running) {
      job.start();
    }
  }
}
