import Category from 'App/Models/Category'
import Redis from '@ioc:Adonis/Addons/Redis'
import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import slugify from 'slugify'
import {CustomAlert} from "Contracts/success/CustomAlert";
import {AlertMessage} from "Contracts/message/AlertMessage";

export class CategoryService {
  private static message: String = 'inside user category service: '

  /**
   * getAllMyCourses all categories
   */
  public static async getAllCategories() {
    Logger.info(this.message + "getting all categories")
    try {
      const cachedCategories = await Redis.get('categories')
      if (cachedCategories) {
        return JSON.parse(cachedCategories)
      }
      const categories = await Category.all()
      await Redis.set('categories', JSON.stringify(categories))
      return categories
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * getAllMyCourses category by id or slug
   */
  public static async getCategoryByIdOrSlug(field: string) {
    Logger.info(this.message + "getting category by id or slug")
    try {
      return await Category.findOrFail(field)
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * add new category category
   */
  public static async addNewCategory(name: string) {
    Logger.info(this.message + "adding new category")
    try {
      const slug: string = slugify(name, {lower: true, replacement: '-'})
      return await Category.create({name, slug})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * edit a category
   */
  public static async editCategory(slug: string, new_value: string) {
    Logger.info(this.message + "editing category")
    try {
      const new_slug: string = slugify(new_value, {lower: true, replacement: '-'})
      const category_updated = await Category.query().where("slug", slug).update({name: new_value, slug: new_slug})
      if (category_updated) {
        return CustomAlert.builder(AlertMessage.CATEGORY_UPDATED)
      }
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }
}
