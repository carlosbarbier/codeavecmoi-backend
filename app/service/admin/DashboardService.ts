import {CourseService} from "App/service/admin/CourseService";
import {HealthNotificationService} from "App/service/HealthNotificationService";

export class DashboardService {

  /**
   * Admin dashboard info
   */
  public static async getDashboardInfo() {
    let service = HealthNotificationService.getInstance();
    service.runJob()
   const max_earning= await CourseService.getBestSellingCourse()
   const {total_course}= await CourseService.getTotalCourse()
   const {total_sell, total_student}= await CourseService.getTotalSellAndTotalStudent()
   const {total_pending}= await CourseService.getTotalPendingCourse()
   const {new_student}= await CourseService.getTotalNewStudent()
   const {new_teacher}= await CourseService.getTotalNewTeacher()

    return Object.assign({
      max_earning,
      total_course,
      total_sell,
      total_student,
      total_pending,
      new_student,
      new_teacher
    })
  }

}
