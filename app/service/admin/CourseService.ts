import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import Course from "App/Models/Course";
import {Status, UserRole} from "Contracts/constant";
import TeacherCourse from "App/Models/TeacherCourse";
import StudentCourse from "App/Models/StudentCourse";
import {DateTimeUtil} from "../../../util/DateTimeUtil";
import User from "App/Models/User";

export class CourseService {
  private static message: string = 'inside course service: '

  /**
   * get all courses
   */
  public static async getAllCourses() {
    try {
      return await Course.query()
        .whereNot({status: Status.CREATED})
        .has('videos')
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getCourse(slug: string) {
    Logger.info(this.message + "getting course  slug")
    try {
      return await Course
        .query()
        .where('slug', slug)
        .preload('videos')
        .firstOrFail()
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async approvedCourse(slug: string) {
    Logger.info(this.message + "approving course")
    try {
      return await Course
        .query()
        .where('slug', slug)
        .update({status: Status.COMPLETED, is_live: true})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async rejectCourse(slug: string, rejectDescription: string) {
    Logger.info(this.message + "rejecting course")
    try {
      return await Course
        .query()
        .where('slug', slug)
        .update({status: Status.REJECTED, reject_description: rejectDescription})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalSellAndTotalStudent() {
    try {
      return await TeacherCourse.query()
        .sum('total_earning as total_sell')
        .sum("total_student as total_student ")
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalCourse() {
    try {
      return await TeacherCourse.query()
        .count('* as total_course')
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalPendingCourse() {
    try {
      const courses = await Course.query()
        .whereNot("status", Status.COMPLETED)
      return Object.assign({
        total_pending: courses.length,
      })
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalRejectedCourse() {
    try {
      return await Course.query()
        .where("status", Status.REJECTED)
        .count('* as total_rejected')

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getBestSellingCourse() {
    try {
      return await TeacherCourse.query()
        .where((builder) => {
          builder.max("total_earning").first()
        })
        .preload("course")
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalNewStudent() {
    try {
      return await StudentCourse.query()
        .where("created_at", ">", DateTimeUtil.yesterday())
        .count('* as new_student')
        .first()

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }

  public static async getTotalNewTeacher() {
    try {
      return await User.query()
        .where('role',UserRole.TEACHER)
        .where("created_at", ">", DateTimeUtil.yesterday())
        .count('* as new_teacher')
        .first()

    } catch (error) {
      Logger.error(error.message);
      throw new InternalServerException("")
    }
  }
}


