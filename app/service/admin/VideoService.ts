import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import AdminVideo from "App/Models/AdminVideo";
import {VideoReviewDto} from "Contracts/dto/VideoReviewDto";

export class VideoService {

  /**
   * review a video
   */
  public static async finishToReviewVideo(dto: VideoReviewDto) {
    try {
      await AdminVideo.create({...dto})
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getVideoAlreadyWatchIds(course_id: number) {
    try {
      return await AdminVideo.query().where("course_id", course_id)
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }
}
