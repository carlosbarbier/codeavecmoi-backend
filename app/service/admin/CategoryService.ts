import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import slugify from "slugify";
import Category from "App/Models/Category";

export class CategoryService {

  /**
   * add new category category
   */
  public static async addNewCategory(name: string) {
    try {
      const slug: string = slugify(name, {lower: true, replacement: '-'})
      return await Category.create({name, slug})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  /**
   * edit a category
   */
  public static async editCategory(slug: string, new_value: string) {
    try {
      const new_slug: string = slugify(new_value, {lower: true, replacement: '-'})
      await Category.query().where("slug", slug).update({name: new_value, slug: new_slug})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

}
