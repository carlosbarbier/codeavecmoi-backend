import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import QuestionAnswer from "App/Models/QuestionAnswer";
import {DateTimeUtil} from "../../../util/DateTimeUtil";

export class QaService {

  /**
   * get all qa from last week
   */
  public static async getAllQaFromLastMonth() {
    try {
      return await QuestionAnswer.query()
        .whereNull("parent_id")
        .andWhere("created_at", ">", DateTimeUtil.month())
        .preload("replies",(builder)=>{
          builder.preload("user")
        })
        .preload("user")
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

}
