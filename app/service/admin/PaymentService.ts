import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import {DateTimeUtil} from "../../../util/DateTimeUtil";
import Payment from "App/Models/Payment";

export class PaymentService {

  /**
   * get all the payments from yesterday
   */
  public static async getAllQaFromYesterday() {
    try {
      return await Payment.query()
        .where("created_at", ">", DateTimeUtil.yesterday())
        .preload("course")
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  /**
   * get all the payments from yesterday
   */
  public static async getAllQaFromLastWeek() {
    try {
      return await Payment.query()
        .where("created_at", ">", DateTimeUtil.week())
        .preload("course")
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }


  /**
   * get all the payments from yesterday
   */
  public static async getAllQaFromLastMonth() {
    try {
      return await Payment.query()
        .where("created_at", ">", DateTimeUtil.month())
        .preload("course")
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

}
