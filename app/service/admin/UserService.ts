import Logger from "@ioc:Adonis/Core/Logger";
import InternalServerException from "App/Exceptions/InternalServerException";
import Course from "App/Models/Course";
import {UserRole} from "Contracts/constant";
import User from "App/Models/User";
import {DateTimeUtil} from "../../../util/DateTimeUtil";
import Database from "@ioc:Adonis/Lucid/Database";

export class UserService {
  private static message: string = 'inside user service: '

  /**
   * get all students
   */
  public static async getAllStudents() {
    try {
      return await User.query()
        .where({role: UserRole.STUDENT})
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  /**
   * get all students
   */
  public static async getAllTeachers() {
    try {
      return Database
        .from('users')
        .where("role", UserRole.TEACHER)

    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  /**
   * get new Students
   */
  public static async getNewStudent() {
    try {
      return await User.query()
        .where({role: UserRole.STUDENT})
        .where({status: true})
        .where("created_at", ">", DateTimeUtil.yesterday())
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  /**
   * get new Teacher
   */
  public static async getNewTeacher() {
    try {
      return await User.query()
        .where({role: UserRole.TEACHER})
        .where({status: true})
        .where("created_at", ">", DateTimeUtil.yesterday())
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getNewTeacherFomLastWeek() {
    try {
      return await User.query()
        .where({role: UserRole.TEACHER})
        .where({status: true})
        .where("created_at", ">", DateTimeUtil.week())
        .orderBy('created_at', 'desc')
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getCourse(slug: string) {
    Logger.info(this.message + "getting course  slug")
    try {
      return await Course
        .query()
        .where('slug', slug)
        .preload('videos')
        .firstOrFail()
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

}
