import {MultipartStream} from "@ioc:Adonis/Core/BodyParser";
import {S3} from "aws-sdk";

import Env from "@ioc:Adonis/Core/Env";
import InternalServerException from "App/Exceptions/InternalServerException";
import Logger from "@ioc:Adonis/Core/Logger";

export class StorageService {

  public static async storeImage(file: MultipartStream) {

    try {
      StorageService.fileNameValidatorForImage(file.file.clientName)
      const params = StorageService.paramsForImage(file)
      return await StorageService.client.upload(params);
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async deleteFile(key: string) {

    try {

      this.client.deleteObject({
        Bucket: Env.get("S3_BUCKET"),
        Key: key,
      }, function (err) {
        if (err) {
          Logger.error(err.message)
        } else {
          Logger.info("picture deleted successfully")
        }
      });
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async updateImage(key: string,file: MultipartStream) {

    try {
      await this.deleteFile(key)
      const params = StorageService.paramsForImage(file)
      return await StorageService.client.upload(params);
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async updateVideo(key: string,file: MultipartStream) {

    try {
      await this.deleteFile(key)
      StorageService.fileNameValidatorForVideo(file.file.clientName)
      const params = StorageService.paramsForVideo(file)
      return await StorageService.client.upload(params);
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }


  public static async storeVideo(file: MultipartStream) {
    try {
      StorageService.fileNameValidatorForVideo(file.file.clientName)
      const params = StorageService.paramsForVideo(file)
      return await StorageService.client.upload(params);
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  private static client = new S3({
    accessKeyId: Env.get("S3_KEY"),
    secretAccessKey: Env.get("S3_SECRET"),
  })

  private static paramsForImage = (file: MultipartStream): any => {
    return {
      Bucket: Env.get("S3_BUCKET"),
      Key: "images/" + Date.now() + "_" + file.file.clientName.replace(/\s/g, ""),
      Body: file,
      CreateBucketConfiguration: {
        LocationConstraint: Env.get("S3_BUCKET"),
      },
    };
  }
  private static paramsForVideo = (file: MultipartStream): any => {
    return {
      Bucket: Env.get("S3_BUCKET"),
      Key: "videos/" + Date.now() + "_" + file.file.clientName.replace(/\s/g, ""),
      Body: file,
      CreateBucketConfiguration: {
        LocationConstraint: Env.get("S3_BUCKET"),
      },
    };
  }

  private static fileNameValidatorForVideo = (filename: string) => {
    const array = ['mp4']
    const ext = filename.split('.')[1]
    if (!array.includes(ext)) {
      throw new InternalServerException('')
    }
  }
  private static fileNameValidatorForImage = (filename: string) => {
    const array = ['jpg', 'png', 'jpeg']
    const ext = filename.split('.')[1]
    if (!array.includes(ext)) {
      throw new InternalServerException('')
    }
  }
}

