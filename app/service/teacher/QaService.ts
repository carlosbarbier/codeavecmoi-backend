import InternalServerException from "App/Exceptions/InternalServerException";
import QuestionAnswer from "App/Models/QuestionAnswer";
import TeacherCourse from "App/Models/TeacherCourse";
import {DateTimeUtil} from "../../../util/DateTimeUtil";

export class QaService {
  public static async getQas(teacher: any) {
    try {
      const ids: number[] = [];
      const courses = await TeacherCourse.query().where("teacher_id", teacher.id)
      courses.map(course => {
        ids.push(course.courseId);
      })
      return await QuestionAnswer.query()
        .whereNull("parent_id")
        .andWhereIn("course_id", ids)
        .preload("user")
        .preload("replies", (query) => {
          query.preload("user")
        })

    } catch (e) {
      throw new InternalServerException("")
    }
  }

  public static async saveQa(qaDto, teacher: any) {

    try {
      await QuestionAnswer.create({
        courseId: qaDto.course_id,
        userId: teacher.id,
        body: qaDto.body,
        parentId: qaDto.parent_id,
      });
      //notify student for reply

    } catch (e) {
      throw new InternalServerException("")
    }
  }

  public static async getTotalQa(ids:Array<number>) {

    try {
      return await QuestionAnswer.query()
        .whereNull("parent_id")
        .andWhereIn("course_id", ids)
        .count('* as total_qas')
        .first()
    } catch (e) {
      throw new InternalServerException("")
    }
  }

  public static async getTotalQaFromYesterday(ids:Array<number>) {
    try {
      return  await QuestionAnswer.query()
        .whereNull("parent_id")
        .where("created_at", ">", DateTimeUtil.yesterday())
        .andWhereIn("course_id", ids)
        .count('* as new_question')
        .first()
    } catch (e) {
      throw new InternalServerException("")
    }
  }


}
