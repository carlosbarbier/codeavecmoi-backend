import InternalServerException from "App/Exceptions/InternalServerException";
import TeacherCourse from "App/Models/TeacherCourse";

import {QaService} from "App/service/teacher/QaService";
import {CourseService} from "App/service/teacher/CourseService";
import {CourseService as StudentCourseService} from "App/service/student/CourseService";

export class DashBoardInfoService {
  public static async getDashboardInfo(teacher: any) {
    const ids: number[] = [];
    let courses;
    try {
      courses = await TeacherCourse.query().where("teacher_id", teacher.id)
      courses.map(course => {
        ids.push(course.courseId);
      })
    } catch (e) {
      console.log(e)
      throw new InternalServerException("")
    }

    const {total_qas} = await QaService.getTotalQa(ids)
    const {new_question} = await QaService.getTotalQaFromYesterday(ids)
    const {earning, total_student} = await CourseService.getTotalEarningAndTotalStudent(teacher)
    const {total_course} = await CourseService.getTotalCourse(teacher)
    const pending = await CourseService.getTotalPendingCourse(teacher)
    const {new_student} = await StudentCourseService.getTotalNewStudent()
    const {rating} = await StudentCourseService.getTotalRating(ids)

    return Object.assign({
      total_qas,
      earning,
      total_student: total_student,
      rating,
      new_student,
      new_question,
      total_course,
      total_pending: pending.length,
    })

  }
}
