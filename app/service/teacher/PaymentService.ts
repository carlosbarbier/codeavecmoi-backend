import InternalServerException from "App/Exceptions/InternalServerException";
import Payment from "App/Models/Payment";

export class PaymentService {
  public static async getPaymentHistory(teacher: any) {
    try {
      return await Payment.query()
        .where("teacher_id", teacher.id)
        .preload("course")
    } catch (e) {
      console.log(e)
      throw new InternalServerException("")
    }
  }

}
