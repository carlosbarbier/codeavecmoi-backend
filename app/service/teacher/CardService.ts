import InternalServerException from "App/Exceptions/InternalServerException";
import Card from "App/Models/Card";
import {CreditCardDto} from "Contracts/dto/CreditCardDto";
import {MaskUtil} from "../../../util/MaskUtil";
import {StripeService} from "App/service/StripeService";

export class CardService {
  public static async saveCard(dto: CreditCardDto, teacher: any) {
    try {
      const cardNumber = MaskUtil.maskNumber(dto.number)
      const cardCvc = MaskUtil.mask(dto.cvc)
      const data = await StripeService.registerTeacherCardInfo(dto, teacher)
      console.log(data)
      await Card.updateOrCreate(
        {teacherId: teacher.id},
        {...dto, number: cardNumber, cvc: cardCvc, teacherId: teacher.id, stripeId: data.id},
      )
    } catch (e) {
      console.log(e)
      throw new InternalServerException("")
    }
  }
}
