import InternalServerException from "App/Exceptions/InternalServerException";
import {ProfileDto} from "Contracts/dto/ProfileDto";
import User from "App/Models/User";
import {StorageService} from "App/service/StorageService";
import {UpdateTeacherProfileDto} from "Contracts/dto/UpdateTeacherProfileDto";

export class ProfileService {
  public static async saveProfile(dto: ProfileDto, s3Response: any, teacher: any) {
    try {
      if (teacher.imageKey) {
        await StorageService.deleteFile(teacher.imageKey)
        await User.query().where("id", teacher.id).update({
          ...dto,
          picture: s3Response.Location,
          image_key: s3Response.Key,
        })
      }
      await User.query().where("id", teacher.id).update({
        ...dto,
        picture: s3Response.Location,
        image_key: s3Response.Key,
      })
    } catch (e) {
      console.log(e.message)
      throw new InternalServerException("")
    }
  }

  public static async updateUserProfile(dto: UpdateTeacherProfileDto, teacher: any) {
    try {
      await User.query().where("id", teacher.id).update({...dto})
    } catch (e) {
      console.log(e.message)
      throw new InternalServerException("")
    }
  }
}
