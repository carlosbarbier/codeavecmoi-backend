import InternalServerException from "App/Exceptions/InternalServerException";
import Logger from "@ioc:Adonis/Core/Logger";
import Course from "App/Models/Course";
import Video from "App/Models/Video";
import {StorageService} from "../StorageService";

const {getVideoDurationInSeconds} = require('get-video-duration')

export class VideoService {

  public static async store(s3Response: any, title: string, slug: string) {
    try {
      const course = await Course.findByOrFail("slug", slug)
      if (course) {
        const video: Video = new Video();
        video.title = title
        video.url = s3Response.Location
        video.videoKey= s3Response.Key
        video.courseId = course.id
        video.duration = await getVideoDurationInSeconds(s3Response.Location)
        await video.save()
        await Course.query()
          .where("slug", slug)
          .increment('duration', video.duration)
          .increment('total_video', 1)
        return await video
      }
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async rearrangeVideosPositions(positions: any[]) {
    try {
      if (positions.length > 0) {
        for (let i = 0; i < positions.length; i++) {
          await Video.query().where("id", positions[i].id).update({position: positions[i].position})
        }
      }
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async delete(id: string) {
    try {
      const video = await Video.query().where("id", id).first()
      if (video && video.videoKey) {
        await StorageService.deleteFile(video.videoKey)
      } else if (video) {
        await video.delete()
      }
    } catch (e) {
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

}
