import InternalServerException from "App/Exceptions/InternalServerException";
import TeacherCourse from "App/Models/TeacherCourse";

export class RevenueService {
  public static async getRevenue(teacher: any) {
    try {
        return await TeacherCourse.query()
          .where("teacher_id",teacher.id)
          .sum('total_earning as earning')
          .first()
    } catch (e) {
      console.log(e)
      throw new InternalServerException("")
    }
  }

}
