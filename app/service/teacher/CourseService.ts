import InternalServerException from "App/Exceptions/InternalServerException";
import Course from "App/Models/Course";

import {CourseLevel, Status} from "Contracts/constant";
import Logger from "@ioc:Adonis/Core/Logger";
import slugify from 'slugify'
import {CourseDto} from "Contracts/dto/CourseDto";
import {DateTimeUtil} from "../../../util/DateTimeUtil";
import {StorageService} from "App/service/StorageService";
import TeacherCourse from "App/Models/TeacherCourse";

export class CourseService {
  private static counter: number = 1

  public static async createCourse(dto: CourseDto, s3Response: any, teacher: any) {
    try {
      const course: Course = CourseService.setCourseProperties(dto, s3Response.Location, s3Response.Key, teacher)
      const courseSaved = await course.save()
      teacher.related('teacher_courses').save(courseSaved)
      return courseSaved.toJSON()
    } catch (e) {
      if (e.code === "ER_DUP_ENTRY") {
        return await CourseService.retryWhenDuplicateKey(dto, s3Response.Location, s3Response.key, teacher)
      }
      Logger.error(e.message)
      await StorageService.deleteFile(s3Response.Key)
      throw new InternalServerException("")
    }
  }

  public static async getAllMyCourses(teacher: any) {
    try {
      return await Course.query()
        .andWhere("teacher_id", teacher.id)
        .preload("category")
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getCourseWithVideos(teacher: any, slug: string) {
    try {

      return await Course.query()
        .where("slug", slug)
        .andWhere("teacher_id", teacher.id)
        .preload("category")
        .preload("videos")
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getCourseWithoutVideos(teacher: any, slug: string) {
    try {
      return await Course.query()
        .where("slug", slug)
        .andWhere("teacher_id", teacher.id)
        .preload("category")
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async activateCourse(teacher: any, slug: string) {
    try {
      return await Course.query()
        .where("slug", slug)
        .andWhere("teacher_id", teacher.id)
        .update({status: Status.COMPLETED})

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async editCourse(user: any, slug: string, dto: any) {
    try {
      if (user) {
        const slug_updated = slugify(dto.title, {lower: true, replacement: '-'})
        return await Course.query().where("slug", slug).update({
          ...dto,
          slug: slug_updated,
          updated_at: DateTimeUtil.now(),
        })
      }
    } catch (e) {
      if (e.code === "ER_DUP_ENTRY") {
        const newSlug = slugify(dto.title + "-" + CourseService.counter, {lower: true, replacement: '-'})
        CourseService.counter++
        return Course.query().where("slug", slug).update({...dto, slug: newSlug, updated_at: DateTimeUtil.now()});
      }
      Logger.error(e.message)
      throw new InternalServerException("")
    }
  }

  public static async getTotalEarningAndTotalStudent(teacher: any) {
    try {
      return await TeacherCourse.query()
        .where("teacher_id", teacher.id)
        .sum('total_earning as earning')
        .sum("total_student as total_student ")
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalCourse(teacher: any) {
    try {
      return await TeacherCourse.query()
        .where("teacher_id", teacher.id)
        .count('* as total_course')
        .first()

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  public static async getTotalPendingCourse(teacher: any) {
    try {
      return await Course.query()
        .where("teacher_id", teacher.id)
        .where("status", Status.CREATED)

    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }

  private static setCourseProperties(dto: CourseDto, url: string, key: string, teacher: any): Course {
    const course: Course = new Course();
    course.slug = slugify(dto.title, {lower: true, replacement: '-'})
    const name_slug = slugify(teacher.name, {lower: true, replacement: '-'})
    const last_name_slug = slugify(teacher.lastName, {lower: true, replacement: '-'})
    course.price = dto.price
    course.level = CourseLevel.BEGINNER
    course.tags = dto.tags
    course.description = dto.description
    course.title = dto.title
    course.url = url
    course.tags = dto.tags
    course.teacherName = teacher.name
    course.teacherEmail = teacher.email
    course.teacherId = teacher.id
    course.teacherNameSlug = name_slug + "-" + last_name_slug
    course.imageKey = key
    course.toLearn = dto.to_learn
    course.categoryId = parseInt(dto.category_id)
    return course
  }

  private static async retryWhenDuplicateKey(dto: CourseDto, url: string, key: string, teacher: any) {
    const course: Course = CourseService.setCourseProperties(dto, url, key, teacher)
    course.slug = slugify(dto.title + "-" + CourseService.counter, {lower: true, replacement: '-'})
    CourseService.counter++
    const courseSaved = await course.save()
    teacher.related('teacher_courses').save(courseSaved)
    return courseSaved.toJSON()
  }

  public static async completed(teacher: any, slug: string) {
    try {
      await Course.query()
        .where("slug", slug)
        .andWhere("teacher_id", teacher.id)
        .update({status: Status.COMPLETED})
    } catch (e) {
      Logger.error(e)
      throw new InternalServerException("")
    }
  }
}
