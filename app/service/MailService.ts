import Mail from '@ioc:Adonis/Addons/Mail'

export class MailService {
  public static async sendEmail (view:string,email:string,subject:string,data:any){
    await Mail.send((message) => {
      message
        .from('info@codeavecmoi.com')
        .to(email)
        .subject(subject)
        .htmlView(view, data)
    })
  }
}
