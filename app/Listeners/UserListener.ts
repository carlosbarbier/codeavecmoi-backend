import { EventsList } from '@ioc:Adonis/Core/Event'
import {MailService} from 'App/service/MailService'

export default class UserListener {
  public async handleRegistration (user: EventsList['new:user']) {
    await MailService.sendEmail('register',user.user.email,'New Signing', user)
  }
  public async handlePasswordForgot (user: EventsList['forgot:password']) {
    await MailService.sendEmail('password',user.user.email,'Password Forgot', user)
  }
}
