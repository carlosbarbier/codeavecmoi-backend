import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {StripeService} from "App/service/StripeService";
import PaymentValidator from "App/Validators/PaymentValidator";
import {CardDto} from "Contracts/dto/CardDto";

export default class PaymentController {

  private message: string = 'inside student payment controller: '

  public async store({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "proceeding payment")
    const cardDto: CardDto = await request.validate(PaymentValidator)
    await StripeService.proceedPayment(cardDto, user)
    response.ok({})
  }
}
