import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {Sanitizer} from "App/Validators/Sanitizer";
import QaValidator from "App/Validators/QaValidator";
import {QaDto} from "Contracts/dto/QaDto";
import {QaService} from "App/service/student/QaService";

export default class VideoController {
  private message: string = 'inside student question answer  controller: '

  public async index({response, params}: HttpContextContract) {
    Logger.info(this.message + "posting a question")
    const slug = Sanitizer.sanitize(params.slug)
    const questions = await QaService.getAllQuestionsAndReplies(slug)
    response.ok(questions)
  }

  public async store({response, params, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "posting a question")
    const slug = Sanitizer.sanitize(params.slug)
    const qaDto: QaDto = await request.validate(QaValidator)
    await QaService.saveQuestionAnswer(slug, qaDto, user)
    response.ok({})
  }
}
