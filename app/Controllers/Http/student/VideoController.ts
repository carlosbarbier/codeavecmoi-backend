import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {VideoService} from "App/service/student/VideoService";
import {Sanitizer} from "App/Validators/Sanitizer";
import VideoIdValidator from "App/Validators/VideoIdValidator";

export default class VideoController {
  private message: string = 'inside student video controller: '

  public async store({response, params, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "finishing to watch a video")
    const slug = Sanitizer.sanitize(params.slug)
    const {video_id} = await request.validate(VideoIdValidator)
    await VideoService.finishToWatchVideo(slug, user, video_id)
    response.ok({})
  }
}
