import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CourseService} from "App/service/student/CourseService";
import {Sanitizer} from "App/Validators/Sanitizer";
import RatingValidator from "App/Validators/RatingValidator";
import {VideoService} from "App/service/student/VideoService";

export default class CourseController {
  private message: string = 'inside student course controller: '

  public async index({response, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "unliking a course")
    const courses = await CourseService.getMyCourses(user)
    response.ok(courses)
  }

  public async enrolForFreeCourse({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "enrolling for free course")
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.enrolForFreeCourse(slug, user)
    response.ok({})
  }

  public async like({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "liking a course")
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.likeCourse(slug, user)
    response.ok({})
  }

  public async unlike({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "unliking a course")
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.unlikeCourse(slug, user)
    response.ok({})
  }

  public async rating({response, params, request, auth: {user}}: HttpContextContract) {
    const rating = await request.validate(RatingValidator)
    Logger.info(this.message + "rating a course")
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.rateCourse(user, slug, rating)
    response.ok({})
  }

  public async completed({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "marking course as a completed")
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.markCourseAsCompleted(user, slug)
    response.ok({})
  }

  public async show({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "watching " + params.slug)
    const slug = Sanitizer.sanitize(params.slug)
    const course = await CourseService.watchCourse(slug, user)
    const ids = await VideoService.getVideoAlreadyWatchIds(course.id, user)
    response.ok({ids, course})
  }
}
