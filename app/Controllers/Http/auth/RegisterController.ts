import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import RegistrationValidator from 'App/Validators/RegistrationValidator'
import {RegistrationDto} from 'Contracts/dto/RegistrationDto'
import {AuthenticationService} from 'App/service/AuthenticationService'
import TeacherRegistrationValidator from "../../../Validators/TeacherRegistrationValidator";
import Logger from "@ioc:Adonis/Core/Logger";

export default class RegisterController {
  /**
   * register new user
   * @param response
   * @param request
   */
  public async register({response, request}: HttpContextContract) {
    const registrationDto: RegistrationDto = await request.validate(RegistrationValidator)
    const user = await AuthenticationService.register(registrationDto)
    response.created(user)
  }

  /**
   * register new user
   * @param response
   * @param request
   */
  public async registerInstructor({response, request}: HttpContextContract) {
    const registrationDto: RegistrationDto = await request.validate(TeacherRegistrationValidator)
    await AuthenticationService.registerTeacher(registrationDto)
    response.ok({})
  }

  /**
   * activate user account
   * @param params
   * @param response
   */
  public async validateEmail({params, response}: HttpContextContract) {
    Logger.info("activate email")
    await AuthenticationService.activateAccount(params.token)
    response.ok({})
  }
}
