import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {AuthenticationService} from 'App/service/AuthenticationService'
import Logger from '@ioc:Adonis/Core/Logger'
import EmailValidator from 'App/Validators/EmailValidator'
import PasswordValidator from 'App/Validators/PasswordValidator'

export default class LoginController {
  private message:string='inside reset password controller : '

  /**
   * user reset password
   * @param response
   * @param request
   * @param auth
   */
  public async forgotPassword ({response, request}: HttpContextContract) {
    Logger.info(this.message+'sending email to user to reset password')
    const {email}= await request.validate(EmailValidator)
     await AuthenticationService.sendEmailToUserToResetPassword(email)
    response.ok({})
  }

  /**
   * reset password
   * @param response
   * @param params
   * @param request
   */
  public async resetPassword ({response, params,request}: HttpContextContract) {
    Logger.info(this.message+'resetting password')
    const {password }=await request.validate(PasswordValidator)
    await AuthenticationService.resetPassword(password,params.token)
    response.ok({})
  }
}
