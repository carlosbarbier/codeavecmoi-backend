import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {AuthenticationService} from 'App/service/AuthenticationService'
import Logger from '@ioc:Adonis/Core/Logger'
import ChangePasswordValidator from "App/Validators/ChangePasswordValidator";
import {ChangePasswordDto} from "Contracts/dto/ChangePasswordDto";

export default class ChangePasswordController {
  private message: string = 'inside student login controller : '

  /**
   * user changing password
   * @param response
   * @param request
   * @param auth
   */
  public async changePassword({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + 'changing password')
    const dto: ChangePasswordDto = await request.validate(ChangePasswordValidator)
    await AuthenticationService.changePassword(dto, user)
    response.ok({})
  }
}
