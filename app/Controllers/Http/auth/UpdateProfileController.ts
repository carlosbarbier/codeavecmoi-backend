import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {AuthenticationService} from 'App/service/AuthenticationService'
import Logger from '@ioc:Adonis/Core/Logger'
import UserProfileValidator from "App/Validators/UserProfileValidator";
import {UpdateProfileDto} from "Contracts/dto/UpdateProfileDto";

export default class UpdateProfileController {
  private message: string = 'inside update profile controller : '

  /**
   *
   * @param response
   * @param request
   * @param user
   */
  public async updateProfile({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + 'updating email or name')
    const profile: UpdateProfileDto = await request.validate(UserProfileValidator)
    await AuthenticationService.changeProfile(profile, user)
    response.ok({})
  }

  public async changePassword({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + 'updating email or name')
    const profile: UpdateProfileDto = await request.validate(UserProfileValidator)
    await AuthenticationService.changeProfile(profile, user)
    response.noContent()
  }
}
