import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import LoginValidator from 'App/Validators/LoginValidator'
import {AuthenticationService} from 'App/service/AuthenticationService'
import {LoginDto} from 'Contracts/dto/LoginDto'
import Logger from '@ioc:Adonis/Core/Logger'

export default class LoginController {
  private message: string = 'inside  login controller : '

  /**
   * user login
   * @param response
   * @param request
   * @param auth
   */
  public async login({response, request, auth}: HttpContextContract) {
    Logger.info(this.message + 'logging in')
    const loginDto: LoginDto = await request.validate(LoginValidator)
    const student = await AuthenticationService.login(loginDto, auth)
    response.ok(student)
  }

  /**
   * verify user
   * @param response
   * @param auth
   */
  public async veryUser({ auth,response}: HttpContextContract) {
    Logger.info(this.message + 'logging in')
    const user = await AuthenticationService.verifyUser( auth)
    response.ok(user)
  }

  /**
   * user logout
   * @param response
   * @param auth
   */
  public async logout({response, auth}: HttpContextContract) {
    Logger.info(this.message + 'logging out')
     await AuthenticationService.logout(auth)
    response.ok({})
  }
}
