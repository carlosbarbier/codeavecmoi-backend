import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import RegistrationValidator from 'App/Validators/RegistrationValidator'
import {RegistrationDto} from 'Contracts/dto/RegistrationDto'
import {AuthenticationService} from 'App/service/AuthenticationService'
import {SocialAccount} from "Contracts/constant";
import SocialAuthValidator from "App/Validators/SocialAuthValidator";


export default class SocialAuthenticationController {
  /**
   * register or login user with google
   * @param response
   * @param request
   * @param auth
   */
  public async loginOrRegisterWithGoogle({response, request, auth}: HttpContextContract) {
    const {role, email, name, password}: RegistrationDto = await request.validate(SocialAuthValidator)
    const user = await AuthenticationService.socialLogin({name, email, password, role}, SocialAccount.GOOGLE, auth)
    response.ok(user)
  }

  /**
   * register or login user with facebook
   * @param response
   * @param request
   * @param auth
   */
  public async loginOrRegisterWithFacebook({response, request, auth}: HttpContextContract) {
    const registrationDto: RegistrationDto = await request.validate(RegistrationValidator)
    const user = await AuthenticationService.socialLogin(registrationDto, SocialAccount.FACEBOOK, auth)
    response.ok(user)
  }

  /**
   * register or login user with github
   * @param response
   * @param request
   * @param auth
   */
  public async loginOrRegisterWithGithub({response, request, auth}: HttpContextContract) {
    const registrationDto: RegistrationDto = await request.validate(RegistrationValidator)
    const user = await AuthenticationService.socialLogin(registrationDto, SocialAccount.GITHUB, auth)
    response.ok(user)
  }
}
