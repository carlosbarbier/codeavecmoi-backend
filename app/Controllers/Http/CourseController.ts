import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CourseService} from "App/service/CourseService";

export default class CourseController {
  private message: string = 'inside category controller: '

  /**
   * getAllMyCourses all the categories
   * @param request
   * @param response
   */

  public async index({request, response}: HttpContextContract) {
    Logger.info(this.message + 'getting  all courses')
    const { page = 1, ...filter } = request.all()
    const courses = await CourseService.getAllCourses(filter,page)
    response.ok(courses)
  }

  /**
   * getAllMyCourses all the categories
   * @param request
   * @param response
   */

  public async allCoursesWithoutPagination({ response}: HttpContextContract) {
    Logger.info(this.message + 'getting  all courses')
    const courses = await CourseService.getAllCoursesWithoutPagination()
    response.ok(courses)
  }

  /**
   * getAllMyCourses a course
   * @param response
   * @param params
   */
  public async show({response, params}: HttpContextContract) {
    Logger.info(this.message + 'getting  a specific course')
    const course = await CourseService.getCourseBySlug( params.slug)
    response.ok(course)
  }

  /**
   * @param response
   * @param params
   */
  public async coursesForHomePage({response}: HttpContextContract) {
    Logger.info(this.message + 'getting  course for homepage')
    const courses = await CourseService.getCoursesForHomePage()
    response.ok(courses)
  }

  /**
   * @param response
   * @param params
   */
  public async coursesByTeacherName({response,params}: HttpContextContract) {
    Logger.info(this.message + 'getting  course fby teacher name')
    const courses = await CourseService.getCourseByTeacherName(params.name)
    response.ok(courses)
  }
}
