import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CategoryService} from 'App/service/CategoryService'

export default class CategoryController {
  private message:string='inside category controller: '

  /**
   * getAllMyCourses all the categories
   * @param response
   */
  public async index ({response}: HttpContextContract) {
    Logger.info(this.message+'getting all categories')
    const categories =await CategoryService.getAllCategories()
    response.ok(categories)
  }
}
