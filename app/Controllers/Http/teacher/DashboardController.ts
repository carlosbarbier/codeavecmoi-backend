import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import {DashBoardInfoService} from "App/service/teacher/DashBoardInfoService";

export default class DashboardController {
  private message: string = 'inside teacher dashboard controller: '

  public async index({response, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "getting dashboard information")
    const info = await DashBoardInfoService.getDashboardInfo(user)
    response.ok(info)
  }
}
