import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import {PaymentService} from "App/service/teacher/PaymentService";

export default class QaController {
  private message: string = 'inside teacher payment controller: '

  public async index({response, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "getting payment history")
    const payments = await PaymentService.getPaymentHistory(user)
    response.ok(payments)
  }
}
