import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import CardValidator from "App/Validators/CardValidator";
import {CardService} from "App/service/teacher/CardService";

export default class QaController {
  private message: string = 'inside teacher card controller: '

  public async store({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "saving payment information")
    const dto = await request.validate(CardValidator)
    await CardService.saveCard(dto, user)
    response.ok({})
  }
}
