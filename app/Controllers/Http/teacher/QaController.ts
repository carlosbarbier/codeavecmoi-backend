import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import {QaService} from "App/service/teacher/QaService";
import TeacherQaValidator from "App/Validators/TeacherQaValidator";

export default class QaController {
  private message: string = 'inside teacher comment controller: '

  public async index({response, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "getting teacher courses comments")
    const qas = await QaService.getQas(user)
    response.ok(qas)
  }

  public async store({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "replying to a question")

    const qaDto = await request.validate(TeacherQaValidator)
    await QaService.saveQa(qaDto, user)
    response.ok({})
  }
}
