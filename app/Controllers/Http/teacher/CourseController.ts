import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CourseService} from "App/service/teacher/CourseService";
import CourseCreationValidator from "App/Validators/CourseCreationValidator";
import {StorageService} from "App/service/StorageService";
import BadRequestException from "App/Exceptions/BadRequestException";
import {CourseDto} from "Contracts/dto/CourseDto";
import {Sanitizer} from "App/Validators/Sanitizer";
import CourseUpdateValidator from "App/Validators/CourseUpdateValidator";

export default class CourseController {
  private message: string = 'inside teacher controller: '

  /**
   * method to fetch all the courses
   * @param response
   * @param auth
   */
  public async index({response, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "getting teacher courses")
    const courses = await CourseService.getAllMyCourses(user)
    response.ok(courses)
  }

  /**
   * method to create a new course
   * @param response
   * @param request
   * @param auth
   */
  public async store({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "creating a new course")

    let S3Response;
    request.multipart.onFile("image", {}, async (file) => {
      const s3 = await StorageService.storeImage(file)
      S3Response = await s3.promise()
    })
    await request.multipart.process();

    const courseDto: CourseDto = await request.validate(CourseCreationValidator)

    if (S3Response !== undefined) {
      const video = await CourseService.createCourse(courseDto, S3Response, user)
      response.created(video)
    } else {
      throw new BadRequestException("")
    }
  }

  /**
   * method to get a single course with the relative videos
   * @param response
   * @param request
   * @param auth
   */
  public async show({response, params, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "getting a specific course")
    const slug = Sanitizer.sanitize(params.slug)
    const {videos} = request.qs()
    if (videos === "true") {
      const course = await CourseService.getCourseWithVideos(user, slug)
      response.ok(course)
    } else {
      const course = await CourseService.getCourseWithoutVideos(user, slug)
      response.ok(course)
    }
  }

  /**
   * method to get a single course with the relative videos
   * @param response
   * @param request
   * @param auth
   */
  public async edit({response, params, auth: {user}, request}: HttpContextContract) {
    Logger.info(this.message + "editing course")
    const slug = Sanitizer.sanitize(params.slug)
    const courseDto: any = await request.validate(CourseUpdateValidator)
    const course = await CourseService.editCourse(user, slug, courseDto)
    response.ok(course)
  }

  /**
   * set course as completed
   * @param response
   * @param request
   */
  public async completed({response, params, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "marking course as completed in order to get reviewed ")
    const slug = Sanitizer.sanitize(params.slug)
     await CourseService.completed(user, slug)
    response.ok({})
  }

}
