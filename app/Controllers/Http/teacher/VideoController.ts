import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CourseService} from "App/service/teacher/CourseService";
import {StorageService} from "App/service/StorageService";
import VideoTitleValidator from "App/Validators/VideoTitleValidator";
import {VideoService} from "App/service/teacher/VideoService";
import BadRequestException from "App/Exceptions/BadRequestException";
import VideoPositionValidator from "App/Validators/VideoPositionValidator";
import {Sanitizer} from "App/Validators/Sanitizer";

export default class VideoController {
  private message: string = 'inside teacher video controller: '

  public async index({response, auth}: HttpContextContract) {
    Logger.info(this.message + "getting teacher courses")
    const courses = await CourseService.getAllMyCourses(auth)
    response.ok(courses)
  }

  /**
   * student protected route
   * @param response
   * @param request

   * @param auth
   */
  public async store({response, request, params}: HttpContextContract) {
    Logger.info(this.message + "adding video")
    let S3Response;
    request.multipart.onFile("video", {}, async (file) => {
      const s3 = await StorageService.storeVideo(file)
      S3Response = await s3.promise()
    })
    await request.multipart.process();
    const {title} = await request.validate(VideoTitleValidator)

    if (S3Response !== undefined) {
      const slug = Sanitizer.sanitize(params.slug)
      const video = await VideoService.store(S3Response, title, slug)
      response.ok(video)
    } else {
      throw new BadRequestException("")
    }
  }

  /**
   * method to rearrange the videos positions
   * @param response
   * @param request
   */
  public async rearrangePositions({response, request}: HttpContextContract) {
    Logger.info(this.message + "rearrange videos positions")
    const {positions} = await request.validate(VideoPositionValidator)
    console.log(positions)
    await VideoService.rearrangeVideosPositions(positions)
    response.ok({})
  }

  public async deleteVideo({response, params}: HttpContextContract) {
    Logger.info(this.message + "deleting video")
    await VideoService.delete(params.id)
    response.ok({})
  }
}
