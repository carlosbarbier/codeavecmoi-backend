import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import {ProfileService} from "App/service/teacher/ProfileService";
import SaveTeacherProfileValidator from "App/Validators/SaveTeacherProfileValidator";
import {StorageService} from "App/service/StorageService";
import BadRequestException from "App/Exceptions/BadRequestException";
import UpdateProfileValidator from "App/Validators/UpdateProfileValidator";

export default class ProfileController {
  private message: string = 'inside teacher profile controller: '

  public async store({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "saving profile")

    let S3Response;

    request.multipart.onFile("picture", {}, async (file) => {
      const s3 = await StorageService.storeImage(file)
      S3Response = await s3.promise()
    })

    await request.multipart.process();
    const dto = await request.validate(SaveTeacherProfileValidator)

    if (S3Response !== undefined) {
      await ProfileService.saveProfile(dto, S3Response, user)
      response.ok({})
    } else {
      throw new BadRequestException("")
    }
  }

  public async update({response, request, auth: {user}}: HttpContextContract) {
    Logger.info(this.message + "updating teacher profile")
    const dto = await request.validate(UpdateProfileValidator)
    await ProfileService.updateUserProfile(dto,  user)
    response.ok({})

  }

}
