import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {PaymentService} from "App/service/admin/PaymentService";

export default class PaymentController {
  private message: string = 'inside admin payment controller: '

  public async index({response}: HttpContextContract) {
    Logger.info(this.message + 'getting all the payment')

    const payments_month = await PaymentService.getAllQaFromLastMonth()
    const payments_week = await PaymentService.getAllQaFromLastWeek()
    const payments_yesterday = await PaymentService.getAllQaFromYesterday()
    const data = Object.assign({
      payments_month,
      payments_week,
      payments_yesterday,
    })
    response.ok(data)
  }
}
