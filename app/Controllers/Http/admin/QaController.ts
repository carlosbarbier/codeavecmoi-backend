import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {QaService} from "App/service/admin/QaService";

export default class QaController {
  private message: string = 'inside admin qa controller '

  public async index({response}: HttpContextContract) {
    Logger.info(this.message + 'getting all questions from last month')
    const qas = await QaService.getAllQaFromLastMonth()
    response.ok(qas)
  }
}
