import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {CourseService} from "App/service/admin/CourseService";
import {Sanitizer} from "App/Validators/Sanitizer";
import RejectCourseValidator from "App/Validators/RejectCourseValidator";
import {VideoService} from "App/service/admin/VideoService";

/**
 * Controller to manage courses
 */
export default class CourseController {
  private message: string = 'inside admin course controller: '

  /**
   * get all the courses
   * @param request
   * @param response
   */

  public async index({response}: HttpContextContract) {
    Logger.info(this.message + 'getting  all courses')
    const courses = await CourseService.getAllCourses()
    response.ok(courses)
  }

  /**
   * get a course
   * @param response
   * @param params
   */
  public async show({response, params}: HttpContextContract) {
    Logger.info(this.message + 'getting  a specific course')
    const slug = Sanitizer.sanitize(params.slug)
    const course = await CourseService.getCourse(slug)
    const ids = await VideoService.getVideoAlreadyWatchIds(course.id)
    response.ok({course, ids})
  }

  /**
   * approve a course
   * @param response
   * @param params
   */
  public async approve({response, params}: HttpContextContract) {
    Logger.info(this.message + 'approving a course')
    const slug = Sanitizer.sanitize(params.slug)
    await CourseService.approvedCourse(slug)
    response.ok({})
  }

  /**
   * reject a course
   * @param request
   * @param response
   * @param params
   */
  public async reject({request, response, params}: HttpContextContract) {
    Logger.info(this.message + 'getting  a specific course')
    const slug = Sanitizer.sanitize(params.slug)
    const {reject_description} = await request.validate(RejectCourseValidator)
    const course = await CourseService.rejectCourse(slug, reject_description)
    response.ok(course)
  }
}
