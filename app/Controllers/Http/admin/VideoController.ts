import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import VideoReviewValidator from "App/Validators/VideoReviewValidator";
import {VideoService} from "App/service/admin/VideoService";

export default class VideoController {
  private message: string = 'inside admin course video: '

  public async store({request, response}: HttpContextContract) {
    Logger.info(this.message + 'saving video after review')
    const dto = await request.validate(VideoReviewValidator)
    await VideoService.finishToReviewVideo(dto)
    response.ok({})
  }
}
