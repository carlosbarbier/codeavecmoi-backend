import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {UserService} from "App/service/admin/UserService";

export default class UserController {
  private message: string = 'inside admin user controller '

  public async teachers({response}: HttpContextContract) {
    Logger.info(this.message + 'getting all the teachers')
    const teachers = await UserService.getAllTeachers()
    response.ok(teachers)
  }

  public async students({response}: HttpContextContract) {
    Logger.info(this.message + 'getting all the students')
    const students = await UserService.getAllStudents()
    response.ok(students)
  }
}
