import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

import Logger from '@ioc:Adonis/Core/Logger'
import {DashboardService} from "App/service/admin/DashboardService";

/**
 * Controller to display all information for the admin
 */
export default class DashboardController {
  private message: string = 'inside admin dashboard controller '

  public async index({response}: HttpContextContract) {
    Logger.info(this.message + 'getting admin info ')
    const info = await DashboardService.getDashboardInfo()
    response.ok(info)
  }
}
