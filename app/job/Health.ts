import { CronJob } from 'cron';
import HealthCheck from "@ioc:Adonis/Core/HealthCheck";

export class Health {

  cronJob: CronJob;

  constructor() {
    this.cronJob = new CronJob("*/10 * * * * *", async () => {
      try {
        await Health.get();
      } catch (e) {
        console.error(e);
      }
    });
    if (!this.cronJob.running) {
      this.cronJob.start();
    }
  }
  public static async get(): Promise<any> {
    const h= await HealthCheck.getReport()
    console.log(h)
  }

}



