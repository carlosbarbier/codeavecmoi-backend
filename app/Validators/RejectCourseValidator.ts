import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class RejectCourseValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    reject_description: schema.string({
      escape: true,
      trim: true
    }),
  })

  public messages = {
    "reject_description.required": "number required",
  }
  public cacheKey = this.ctx.routeKey
}

