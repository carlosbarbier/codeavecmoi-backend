import { schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class PasswordValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    password: schema.string({},),
  })

  public messages = {
    'password.required': 'password required',
  }
  public cacheKey = this.ctx.routeKey
}
