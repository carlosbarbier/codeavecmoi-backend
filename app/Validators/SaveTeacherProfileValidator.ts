import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class SaveTeacherProfileValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    name: schema.string({
      escape: true,
      trim: true,
    }),
    last_name: schema.string({
      escape: true,
      trim: true,
    }),
    about: schema.string({
      escape: true,
      trim: true,
    }),

    occupation: schema.string({
      escape: true,
      trim: true,
    }),
    facebook: schema.string.optional({}, [
      rules.url()
    ]),
    instagram: schema.string.optional({}, [
      rules.url()
    ]),
    youtube: schema.string.optional({}, [
      rules.url()
    ]),
    twitter: schema.string.optional({}, [
      rules.url()
    ]),
    website: schema.string.optional({}, [
      rules.url()
    ]),
  })

  public messages = {
    required: "{{field}} required",
  }
  public cacheKey = this.ctx.routeKey
}






