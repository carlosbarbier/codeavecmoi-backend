import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class TeacherQaValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    parent_id: schema.number.optional([
      rules.exists({ table: 'question_answers', column: 'id' })
    ]),
    body: schema.string({
      escape: true,
      trim: true,
    }),
    course_id: schema.number(),
  })

  public messages = {
    required: "{{field}} required",
  }
  public cacheKey = this.ctx.routeKey
}
