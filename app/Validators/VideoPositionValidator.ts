import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class VideoPositionValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    positions: schema
      .array([])
      .members(schema.object().members({
        id: schema.number(),
        position: schema.number(),
      }))
  })

  public messages = {
    'positions.array': 'invalid input type',
  }
  public cacheKey = this.ctx.routeKey
}
