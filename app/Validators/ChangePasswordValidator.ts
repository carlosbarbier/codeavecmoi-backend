import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class ChangePasswordValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    password: schema.string({},[rules.minLength(6), rules.confirmed()]),
    current_password: schema.string({},[rules.minLength(6)]),
  })

  public messages = {
    "password.required": "password required",
    "password_confirmation.required": "password confirmation required",
    "current_password.required": "current password required",
  }
  public cacheKey = this.ctx.routeKey
}
