import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class RegistrationValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    email: schema.string({}, [rules.email(), rules.unique({
      table: 'users',
      column: 'email',
    })]),
    role: schema.string.optional(),
    id: schema.string.optional(),
    name: schema.string({}),
    password: schema.string({}, [rules.minLength(6)]),
  })

  public messages = {
    'email.required': 'email required',
    'email.unique': 'email taken',
    'name.required': 'name required',
    'email.email': 'invalid email',
    'password.required': 'password required',
    'password.minLength': 'minimum password length 6 characters',
  }
  public cacheKey = this.ctx.routeKey
}
