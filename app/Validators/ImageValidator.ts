import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
export default class ImageValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    image: schema.file({
      size: '2mb',
      extnames: ['jpg', 'png', 'jpeg'],
    })
  })

  public messages = {
    'image.required': 'image required',
    'image.size ':'image size must be less than 2MB',
    'image.extnames ':'image must be jpeg,png or jpeg',
  }
  public cacheKey = this.ctx.routeKey
}
