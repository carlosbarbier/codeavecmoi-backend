import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
export default class VideoTitleValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    title: schema.string({
      escape: true,
      trim: true
    }),
  })

  public messages = {
    'title.required': 'title required',
  }
  public cacheKey = this.ctx.routeKey
}







