import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class LoginValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    email: schema.string({}, [rules.email()]),
    password: schema.string({}),
  })

  public messages = {
    'email.required': 'name required',
    'email.email': 'invalid email',
    'password.required': 'password required',
  }
  public cacheKey = this.ctx.routeKey
}
