import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
export default class EmailValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    email: schema.string({}, [rules.email()]),
  })

  public messages = {
    'email.required': 'email required',
    'email.email': 'invalid email',
  }
  public cacheKey = this.ctx.routeKey
}
