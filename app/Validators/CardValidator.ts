import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class CardValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    number: schema.string({}, [rules.maxLength(19)]),
    expiry: schema.string({}, [rules.maxLength(4)]),
    cvc: schema.string({}, [rules.maxLength(3)]),
    name: schema.string({}),
  })

  public messages = {
    "number.required": "number required",
    "name.required": "name required",
    "expiry.required": "expiration date and month required",
    "cvc.required": "cvc required",
  }
  public cacheKey = this.ctx.routeKey
}

