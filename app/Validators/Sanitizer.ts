const sanitizer = require('sanitizer');
export class Sanitizer {
  public static sanitize(data: string): string {
    return sanitizer.sanitize( sanitizer.escape(data));
  }
}
