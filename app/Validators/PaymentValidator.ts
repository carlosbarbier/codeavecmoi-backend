import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class PaymentValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    number: schema.string({},[rules.maxLength(19)]),
    exp_month: schema.string({},[rules.maxLength(2)]),
    exp_year: schema.string({},[rules.maxLength(4)]),
    cvc: schema.string({},[rules.maxLength(3)]),
    amount: schema.number(),
    slugs: schema.array().members(
      schema.string()
    )
  })

  public messages = {
    "number.required": "number required",
    "exp_month.required": "expiration month required",
    "exp_year.required": "expiration year required",
    "cvc.required": "cvc required",
    "slugs.required": "slugs required",
  }
  public cacheKey = this.ctx.routeKey
}

