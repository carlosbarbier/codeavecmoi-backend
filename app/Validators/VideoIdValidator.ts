import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
export default class VideoIdValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    video_id: schema.number([
      rules.exists({ table: 'videos', column: 'id' })
    ])
  })

  public messages = {
    'video_id.required': 'we can not find the actual video',
  }
  public cacheKey = this.ctx.routeKey
}
