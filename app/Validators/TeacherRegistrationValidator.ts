import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class TeacherRegistrationValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    email: schema.string({}, [rules.email(), rules.unique({
      table: 'users',
      column: 'email',
    })]),
    role: schema.string.optional(),
    name: schema.string({}),
    last_name: schema.string({}),
    password: schema.string({}, [rules.minLength(6)]),
  })

  public messages = {
    'email.required': 'email required',
    'last_name.required': 'last name required',
    'email.unique': 'email taken',
    'name.required': 'name required',
    'email.email': 'invalid email',
    'password.required': 'password required',
    'password.minLength': 'minimum password length 6 characters',
  }
  public cacheKey = this.ctx.routeKey
}
