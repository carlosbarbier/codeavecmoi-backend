import {rules, schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
export default class UserProfileValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    email: schema.string({}, [rules.email()]),
    name: schema.string({}),
  })

  public messages = {
    required: "{{field}} required",
    string: `invalid input for field {{field}} `,
    email: `invalid email for field {{field}} `,
  }
  public cacheKey = this.ctx.routeKey
}
