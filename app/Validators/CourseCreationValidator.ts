import { schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {CourseLevel} from "Contracts/constant";


export default class CourseCreationValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    title: schema.string({
      escape: true,
      trim: true
    }),
    description: schema.string({
      escape: true,
      trim: true,
    }),
    price: schema.number(),
    tags: schema.string({}),
    to_learn: schema.string({}),
    category_id: schema.string({}),
    level: schema.enum([CourseLevel.ALL,CourseLevel.ADVANCED,CourseLevel.BEGINNER,CourseLevel.INTERMEDIARY] as const),
  })

  public messages = {
    required: "{{field}} required",
    string: `invalid input for field {{field}} `,
    number: `invalid input for field {{field}} expect numbers`,
    array: `invalid input for  {{field}} `,
    'image.required': 'image required',
    'file.size':'image size must be less than 2MB',
  }
  public cacheKey = this.ctx.routeKey
}






