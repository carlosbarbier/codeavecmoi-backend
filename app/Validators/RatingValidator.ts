import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class RatingValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    rating: schema.enum([1, 2, 3, 4, 5] as const),
    comment_body: schema.string.optional({
      escape: true,
      trim: true
    }),
  })

  public messages = {
    required: "{{field}} required",
  }
  public cacheKey = this.ctx.routeKey
}
