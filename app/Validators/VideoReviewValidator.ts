import {schema} from '@ioc:Adonis/Core/Validator'
import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'

export default class VideoReviewValidator {
  constructor(protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    observation: schema.string.optional({
      escape: true,
      trim: true
    }),
    video_id:schema.number(),
    course_id:schema.number()
  })

  public messages = {
    required: "{{field}} required",
  }
  public cacheKey = this.ctx.routeKey
}

