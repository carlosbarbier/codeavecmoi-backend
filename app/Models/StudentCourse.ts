import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'
import {DateTime} from "luxon";


export default class StudentCourse extends BaseModel {
  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true,serializeAs: null})
  public updatedAt: DateTime

  @column({serializeAs: null})
  public studentId: number

  @column()
  public courseId: number

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isCompleted: boolean

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public wishlist: boolean

  @column({serializeAs: null})
  public isPaid: boolean

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isArchived: boolean

  @column({serializeAs: null})
  public progress: number

  @column({serializeAs: null})
  public isActive: boolean

  @column()
  public rating: number

  @column()
  public commentBody: string

  @column()
  public author: string

  @column()
  public commentDate: DateTime

}
