import {BaseModel, BelongsTo, belongsTo, column} from '@ioc:Adonis/Lucid/Orm'
import Course from "App/Models/Course";
import User from "App/Models/User";

export default class TeacherCourse extends BaseModel {

  @column()
  public teacherId: number

  @column()
  public courseId: number

  @column()
  public totalStudent: number

  @column()
  public totalEarning: number

  @belongsTo(() => Course)
  public course: BelongsTo<typeof Course>

  @belongsTo(() => User, {
    foreignKey: 'teacherId',
  })
  public teacher: BelongsTo<typeof User>

}
