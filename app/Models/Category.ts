import { DateTime } from 'luxon'
import {BaseModel, column, hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import Course from "App/Models/Course";


export default class Category extends BaseModel {
  @column({ isPrimary: true})
  public id: number

  @column()
  public name: string

  @column()
  public slug: string

  @column.dateTime({ autoCreate: true ,serializeAs: null})
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,serializeAs: null })
  public updatedAt: DateTime

  @hasMany(() => Course)
  public courses: HasMany<typeof Course>
}
