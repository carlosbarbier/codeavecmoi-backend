import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Card extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public number: string

  @column()
  public name: string

  @column()
  public cvc: string

  @column()
  public expiry: string

  @column()
  public teacherId: number

  @column()
  public stripeId: string

}
