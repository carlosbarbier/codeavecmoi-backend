import { DateTime } from 'luxon'
import {BaseModel, BelongsTo, belongsTo, column} from '@ioc:Adonis/Lucid/Orm'
import Course from "App/Models/Course";

export default class Video extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true,serializeAs: null })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,serializeAs: null })
  public updatedAt: DateTime

  @column()
  public title: string

  @column()
  public url: string

  @column()
  public duration: number

  @column()
  public position?: number

  @column()
  public courseId: number

  @column()
  public videoKey?: string

  @belongsTo(() => Course)
  public course: BelongsTo<typeof Course>

}
