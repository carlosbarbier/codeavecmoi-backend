import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class StudentVideo extends BaseModel {

  @column.dateTime({ autoCreate: true,serializeAs:null })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true,serializeAs:null })
  public updatedAt: DateTime

  @column({serializeAs: null})
  public studentId: number

  @column({serializeAs: null})
  public courseId: number

  @column({serializeAs: "id"})
  public videoId: number

  @column({serializeAs: null})
  public isCompleted: boolean
}
