import {DateTime} from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {BaseModel, beforeSave, column, manyToMany, ManyToMany} from '@ioc:Adonis/Lucid/Orm'

import Course from "App/Models/Course";

export default class User extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column()
  public email: string

  @column()
  public name: string

  @column()
  public nameSlug: string

  @column()
  public role: string

  @column({serializeAs: null})
  public password: string

  @column({serializeAs: null})
  public token?: string

  @column({serializeAs: null})
  public source?: string

  @column({
  serialize: (value) => {
    return value == 1
  }})
  public isActive?: boolean

  @column()
  public lastName?: string

  @column()
  public occupation: string

  @column()
  public about?: string

  @column()
  public picture?: string

  @column()
  public facebook?: string

  @column()
  public youtube?: string

  @column()
  public website?: string

  @column()
  public instagram?: string

  @column()
  public twitter?: string

  @column()
  public imageKey?: string

  @column.dateTime({autoCreate: true, serializeAs: null})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true, serializeAs: null})
  public updatedAt: DateTime

  @manyToMany(() => Course, {
    pivotTable: 'teacher_courses',
    localKey: 'id',
    pivotForeignKey: 'teacher_id',
  })
  public teacher_courses: ManyToMany<typeof Course>

  @manyToMany(() => Course, {
    pivotTable: 'student_courses',
    localKey: 'id',
    pivotForeignKey: 'student_id',
    pivotColumns: ["is_completed", "wishlist", "is_archived", "progress", "rating", "is_active", "comment_body", "comment_date"],
  })
  public student_courses: ManyToMany<typeof Course>

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

}
