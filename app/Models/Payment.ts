import {DateTime} from 'luxon'
import {BaseModel, BelongsTo, belongsTo, column} from '@ioc:Adonis/Lucid/Orm'
import Course from "App/Models/Course";

export default class Payment extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public studentId: number

  @column({serializeAs: null})
  public teacherId: number

  @column()
  public courseId: number

  @column({serializeAs: null})
  public stripeId: string

  @column()
  public amount: number

  @column({serializeAs: null})
  public receiptUrl: string

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isPaid: boolean

  @column()
  public status: string

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isRefunded:boolean

  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime

  @belongsTo(() => Course)
  public course: BelongsTo<typeof Course>
}
