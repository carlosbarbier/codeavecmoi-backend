import {BaseModelFilter} from '@ioc:Adonis/Addons/LucidFilter'
import {ModelQueryBuilderContract} from '@ioc:Adonis/Lucid/Orm'
import Course from 'App/Models/Course'

export default class CourseFilter extends BaseModelFilter {
  public $query: ModelQueryBuilderContract<typeof Course, Course>

  category(id: string) {
    this.$query.whereIn('category_id', id.split(","))
  }

  price(price: number | string) {
    if (price === "free") {
      this.$query.where('price', 0).preload('category')
    } else {
      this.$query.where('price', '>', 0).andWhere('price', '<=', price)
    }

  }

  level(level: string) {
    if(level==="all"){
      this.$query.where('level', "all level")
    }else{
      this.$query.where('level', level)
    }
  }

  search(searchable: string) {
    if (searchable !== "") {
      this.$query.where('title', 'LIKE', `%${searchable}%`)
        .orWhere('description', 'LIKE', `%${searchable}%`)
    }
  }

  order(order: string) {
    if (order === "recent") {
      this.$query.preload('category').orderBy('created_at', 'desc')
    }
  }
}
