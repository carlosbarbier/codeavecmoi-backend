import {DateTime} from 'luxon'
import {
  afterFetch,
  afterFind,
  BaseModel,
  beforeUpdate,
  belongsTo,
  BelongsTo,
  column,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'

import Category from "App/Models/Category";
import Video from "App/Models/Video";
import {filterable} from "@ioc:Adonis/Addons/LucidFilter";
import CourseFilter from "App/Models/Filters/CourseFilter";
import QuestionAnswer from "App/Models/QuestionAnswer";
import StudentCourse from "App/Models/StudentCourse";
import {Status} from "Contracts/constant";

@filterable(CourseFilter)
export default class Course extends BaseModel {
  public serializeExtras = true
  @column({isPrimary: true})
  public id: number

  @column()
  public title: string

  @column()
  public slug: string

  @column()
  public teacherNameSlug: string

  @column()
  public description: string

  @column({serializeAs: null})
  public isPro?: boolean

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isPremium?: boolean

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isFeature?: boolean

  @column({
    serialize: (value) => {
      return value == 1
    }})
  public isLive?: boolean

  @column()
  public price: number

  @column()
  public occupation: string

  @column()
  public level: string

  @column()
  public status: Status

  @column()
  public url?: string

  @column()
  public duration?: bigint

  @column()
  public tags: any

  @column.dateTime({
    autoCreate: true,
  })
  public createdAt: DateTime

  @column.dateTime({
    autoCreate: true, autoUpdate: true,
  })
  public updatedAt: DateTime

  @column()
  public categoryId: number

  @column()
  public toLearn: any

  @column()
  public rejectDescription: string

  @column()
  public rating?: number

  @column()
  public teacherName: string

  @column()
  public teacherId: number

  @column({serializeAs: null})
  public teacherEmail: string

  @column()
  public ratingTotalSum?: number

  @column()
  public imageKey?: string

  @belongsTo(() => Category)
  public category: BelongsTo<typeof Category>

  @hasMany(() => QuestionAnswer)
  public questions_answers: HasMany<typeof QuestionAnswer>

  @hasMany(() => Video)
  public videos: HasMany<typeof Video>

  @hasMany(() => StudentCourse)
  public students: HasMany<typeof StudentCourse>

  @afterFetch()
  public static afterFetchHook(courses: Course[]) {
    courses.map(course => {
      course.tags = course.tags.split(",")
      course.toLearn = course.toLearn.split(",")
    })
  }

  @afterFind()
  public static afterFindHook(course: Course) {
    course.tags = course.tags.split(",")
    course.toLearn = course.toLearn.split(",")
  }

  @beforeUpdate()
  public static beforeUpdateHook(course: Course) {
    course.updatedAt = DateTime.local()
  }
}
