import {DateTime} from 'luxon'
import {BaseModel, belongsTo, BelongsTo, column, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import User from "App/Models/User";

export default class QuestionAnswer extends BaseModel {
  @column({isPrimary: true})
  public id: number
  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime

  @column()
  public categoryId: number

  @column()
  public courseId: number

  @column()
  public parentId?: number

  @column()
  public userId: number

  @column()
  public body: string

  @belongsTo(() => QuestionAnswer, {
    foreignKey: 'parentId',
  })
  public parent: BelongsTo<typeof QuestionAnswer>

  @hasMany(() => QuestionAnswer, {
    localKey: 'id',
    foreignKey: 'parentId',
  })
  public replies: HasMany<typeof QuestionAnswer>

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>

}
