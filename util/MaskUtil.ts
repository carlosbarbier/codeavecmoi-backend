export class MaskUtil {

  public static maskNumber(number:string) {
    return number.replace(/.(?=.{4})/g, "X");
  }

  public static mask(value:string) {

    return value.replace(/[0-9]/g, "X");
  }
}
