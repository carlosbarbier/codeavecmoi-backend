import {DateTime} from "luxon";
import moment from "moment";

export class DateTimeUtil {
  public static now(): string {
    return DateTime.local().toString().replace("T", " ").slice(0, 19)
  }

  public static yesterday():string{
    return moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss')
  }

  public static week():string{
    return moment().subtract(7, 'days').format('YYYY-MM-DD HH:mm:ss')
  }
  public static month():string{
    return moment().subtract(30, 'days').format('YYYY-MM-DD HH:mm:ss')
  }

  public static year():string{
    return moment().subtract(30, 'days').format('YYYY-MM-DD HH:mm:ss')
  }
}
